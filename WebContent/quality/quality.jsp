﻿<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<%
 double d = Math.random();
String flag = Double.toString(d);
session.setAttribute("flag",flag);
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>test</title>
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/quality.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/my.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/plug-in/chosen_select/chosen.css">
	<link href="<%=path%>/plug-in/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<script src="<%=path%>/js/jquery-3.3.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/layer/layer.js" charset="UTF-8"></script>
	

	
</head>

<body>
	<div class="container">
		<div class="row clearfix">
			<div class="page-header">
				<h1 style="float: left;">
					<small>欢迎登陆</small> 产保中心 
				</h1>
				<div  style="float: right; margin: 20px; margin-bottom:0px;">
						<div >
						<div class="navbar-header">
							<a class="navbar-brand" href="">${sessionScope.loginUserName}</a>
						</div>
						<ul class="nav navbar-nav">
							<li><a href="<%=path%>/users/Users_modifyPassword.action">修改密码</a></li>
							<s:if test="#session.loginUserAuth == 1">
							<li><a href="<%=path%>/users/Users_admin.action">用户管理</a></li>
							</s:if>
							<li><a href="<%=path%>/users/Users_logout.action">退出</a></li>
						</ul>
					</div>
		        </div>
		        
		        <div style="clear: both;"></div>
			</div>

			<div class="col-md-2 column">
				<div class="panel-group" id="panel-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a class="panel-title collapsed" href="#panel-element-1" data-toggle="collapse" data-parent="#panel-1" onclick="redirect('weekly')">
							    <span class="glyphicon glyphicon-edit"></span>&nbsp;周报
							</a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-1">
							<div class="panel-body">
								<ul>
									<li>
										<a href="./index.html">我的周报</a>
									</li>
									<li>
										<a href="./index.html">其他周报</a>
									</li>
									<li>
										<a href="./index.html">新建周报</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-2" data-toggle="collapse" data-parent="#panel-1">
							 	<span class="glyphicon glyphicon-music"></span>&nbsp;团建活动
							 </a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-2">
							<div class="panel-body">
								<ul>
									<li>
										<a href="./event.html">活动展示</a>
									</li>
									<li>
										<a href="./event.html">通知公告</a>
									</li>
									<li>
										<a href="event.html">人员信息</a>
									</li>
									<li>
										<a href="event.html">学习资料</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-3" data-toggle="collapse" data-parent="#panel-1">
							 	<span class="glyphicon glyphicon-tasks"></span>&nbsp;质量管理
							 </a>
						</div>
						<div class="panel-collapse collapse in" id="panel-element-3">
							<div class="panel-body">
								<ul>
									<li>
										<a href="#" class="quality-list">配置管理</a>
									</li>
									<li>										
										<a href="#" class="quality-list">质量保证</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-4" data-toggle="collapse" data-parent="#panel-1" onclick="redirect('test')">
							 	<span class="glyphicon glyphicon-th-list"></span>&nbsp;项目测试
							 </a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-4">
							<div class="panel-body">
								<ul>
									<li>
										<a href="./protest.html">测试项目</a>
									</li>
									<li>
										<a href="./protest.html">测试数据</a>
									</li>
									<li>
										<a href="./protest.html">测试人员</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-10 column">
				<div class="tabbable" id="tabs-941325">
					<ul class="nav nav-tabs" id="report-nav">
						<li class="active nav-quality">
							 <a href="#panel-31 " data-toggle="tab" >配置管理</a>
						</li>
						<li class="nav-quality">
							 <a href="#panel-32" data-toggle="tab">质量保证</a>
						</li>
						
					</ul>
					<div class="tab-content">
						
						<div class="tab-pane active" id="panel-31">
							<div style="max-height:420px;padding-top:10px;">
								<table class="table-PZGL" >
								<div style="padding-top:3px; padding-left:613px;padding-bottom:23px;float:left;">
								<button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal-PZGL-1">新增</button>
									<button type="button" class="btn btn-info o-btn-info" style="margin-left:23px;">全选</button>
								<button type="button" class="btn btn-warning m-btn-del" style="margin-left:23px;">删除</button>
								<button type="button" class="btn btn-primary m-btn-export" style="margin-left: 20px;">导出</button>
							</div><br>
								  <thead>
								    <tr>	
								      <th >操作</th>		     						      
								      <!-- <th >序号</th> -->
								      <th >项目名称</th>
								      <th >活动</th>
								      <th >活动类型</th>
								      <th >文档/代码名称</th>
								      <th >版本号</th>								      
								      <th >实际建立日期</th>
								     <!-- <th >存放位置</th>
								     <th >备注</th> -->
								      
								    </tr>		    
								  </thead>
								 	
								  <tbody>
								   <s:iterator value="#session.management_list" var="mgt">
									  <tr class="list">	  		
									  		<td><input type="checkbox" class="m-checkbox" value="<s:property value="#mgt.id" />"></td>
									    	<%-- <td><s:property value="#mgt.id"/></td>	 --%>						    	
									        <td><a href="#" data-toggle="modal" data-target="#modal-PZGL" managementid="<s:property value="#mgt.id" />" class="management-projectName" id="management-projectName-<s:property value="#mgt.id" />"><s:property value="#mgt.projectName" /></a></td> 
									    	<td><s:property value="#mgt.active"/></td>
									    	<td><s:property value="#mgt.activeType"/></td>
									    	<td><s:property value="#mgt.documentName"/></td>
									    	<td><s:property value="#mgt.number"/></td>
									    	<td><s:property value="#mgt.date"/></td>
									    	<%--<td><s:property value="#mgt.address"/></td>
									    	 <td><s:property value="#mgt.remark"/></td>  --%>
									    	
									    </tr>
									    	 </s:iterator>
									</tbody>
								
								 
								</table>

								
								
								<br>
							</div>
							
						
                      		
                      		<div class="modal" id="modal-PZGL">
								<div class="modal-dialog">
									<div class="modal-content" style="height:100%;">
										<input type="hidden" value="" id="modal-management-id">
										<div class="modal-body">
											<form role="form" action="<%=path%>/management/Management_addmanagement.action" method="post">			
					                      		<div class="form-div">
					                      			<label for="pro-field">项目名称</label>
					                      			<div >
						                      			<div class="form-div-1" style="width:70%;float:left;margin-left:20px">
							                      			<!--select data-placeholder="Choose a Country..." class="select" tabindex="1" id="select-xmmc-1" style="display:block">
							                      				<option>111</option>
							                      				<option>222</option>
							                      				<option>333</option>
							                      			</select-->
															<input class="form-control"  id="modal-management-projectName"  name="projectName">
						                      			</div>
						                      			
					                      			</div>
					                      		</div>	
												<div class="form-div">
													<div class="hd-div">
							                      		<label>活动</label>
							                      		<div >
							                      			<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="modal-management-active" name="active">
							                      				<option>出库</option>
							                      				<option>入库</option>
							                      				<option>发布基线</option>
							                      			</select>                      			
						                      			</div>
					                      			</div>
					                      			
					                      			<div class="hd-div">
						                      			<label>活动类型</label>
						                      			<div>
							                      			<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="modal-management-activeType" name="activeType">
							                      				<option>初始入库</option>
							                      				<option>变更后入库</option>
							                      				<option>功能基线</option>
							                      				<option>分配基线</option>
							                      				<option>产品基线</option>
							                      			</select>
						                      			</div>
					                      			</div>
					                      		</div>	
												<div class="form-div">
													<div class="hd-div">
							                      		<label>文档/代码名称</label>
							                      		<div>
							                      			<input class="form-control" id="modal-management-documentName" name="documentName">
							                      		</div>
							                      	</div>	
						                      		<div class="hd-div">
						                      			<label for="num-field">版本号</label>
						                      			<div>
							                      			<input class="form-control" id="modal-management-number" name="number">
							                      		</div>
							                      	</div>
					                      		</div>	
												<div class="form-div">	
													<div class="hd-div">
							                      		<label>实际建立时间</label>
							                      		<div >
							                      			<div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd" style="margin-left:0px;">
											                    <input class="form-control" size="16" id="date1" style="width:150px" type="text" value="" readonly name="date">
											                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
																<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										                	</div>
															<input type="hidden" id="" value="" name="date"/><br/>
							                      		</div>
							                      	</div>	
													<div class="hd-div">	
							                      		<label>输出物存放位置</label>
							                      		<div >
							                      			<input class="form-control" id="modal-management-address" name="address">
							                      		</div>	
						                      		</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div>
					                      				<label>备注</label>
					                      				<div>
					                      					<textarea id="modal-management-remark" name="remark"></textarea>
					                      				</div>
					                      			</div>
					                      		</div>
				                      	
			                      	
			                      		<div class="modal-footer">
				                      			<div style="position:absolute;left:40%;">
					                      		    <button class="btn btn-default" id="" type="submit">新建</button>
					                      			<button class="btn btn-default" type="button" data-dismiss="modal"  id="modal-submit-1" >更改</button> 
					                      			<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				                      			</div>
				                      	</div> 
				                      	</form>
				                     </div>
			                      	</div>
		                      	</div>
                      		</div>  
  
							
                      			<div class="modal" id="modal-PZGL-1">
								<div class="modal-dialog">
									<div class="modal-content" style="height:100%;">
										<div class="modal-body">
											<form role="form" action="<%=path%>/management/Management_addmanagement.action" method="post">			
					                      		<div class="form-div">
					                      			<label for="pro-field">项目名称</label>
					                      			<div >
						                      			<div class="form-div-1" style="width:70%;float:left;margin-left:20px">
							                      			<!--select data-placeholder="Choose a Country..." class="select" tabindex="1" id="select-xmmc-1" style="display:block">
							                      				<option>111</option>
							                      				<option>222</option>
							                      				<option>333</option>
							                      			</select-->
															<input class="form-control"  id="modal-management-projectName"  name="projectName">
						                      			</div>
						                      			
					                      			</div>
					                      		</div>	
												<div class="form-div">
													<div class="hd-div">
							                      		<label>活动</label>
							                      		<div >
							                      			<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="hudong" name="active">
							                      				<option>出库</option>
							                      				<option>入库</option>
							                      				<option>发布基线</option>
							                      			</select>
							   
						                      			</div>
					                      			</div>
					                      			<div class="hd-div">
						                      			<label>活动类型</label>
						                      			<div>
							                      			<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="huodongleixin" name="activeType"> 
							                      				<option>初始入库</option>
							                      				<option>变更后入库</option>
							                      				<option>功能基线</option>
							                      				<option>分配基线</option>
							                      				<option>产品基线</option>
							                      			</select>
						                      			</div>
					                      			</div>
					                      		</div>	
												<div class="form-div">
													<div class="hd-div">
							                      		<label>文档/代码名称</label>
							                      		<div>
							                      			<input class="form-control" id="" name="documentName">
							                      		</div>
							                      	</div>	
						                      		<div class="hd-div">
						                      			<label>版本号</label>
						                      			<div>
							                      			<input class="form-control" id="" name="number">
							                      		</div>
							                      	</div>
					                      		</div>	
												<div class="form-div">	
													<div class="hd-div">
							                      		<label>实际建立时间</label>
							                      		<div >
							                      			<div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd" style="margin-left:0px;">
											                    <input class="form-control" size="16" id="date"  style="width:150px" type="text" value="" name="date"  readonly>
											                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
																<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										                	</div>
															<input type="hidden" id="date" value="" /><br/>
							                      		</div>
							                      	</div>	
													<div class="hd-div">	
							                      		<label>输出物存放位置</label>
							                      		<div >
							                      			<input class="form-control" id="" name="address">
							                      		</div>	
						                      		</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div>
					                      				<label>备注</label>
					                      				<div>
					                      					<textarea name="remark"></textarea>
					                      				</div>
					                      			</div>
					                      		</div>
					                      		<input type="hidden" name="flag" value="<%=flag%>">
					                      			 		<div class="modal-footer">
				                      			<div style="position:absolute;left:40%;">
					                      			<button class="btn btn-default" id="Save-1" type="submit">保存</button>
					                      			
					                      			<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				                      			</div>
				                      	</div>
				                      	</form>
				                     </div>
			                      	</div>
		                      	</div>
                      		</div>    
                      		</div>
						<div class="tab-pane" id="panel-32">	
                      		<div style="max-height:420px;padding-top:10px; ">
								<table class="table-ZLBZ" >
								<div style="padding-top:3px; padding-left:613px;  padding-bottom:23px;float:left;">
									<button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal-ZLBZ-1">新增</button>
									<button type="button" class="btn btn-info m-btn-info" style="margin-left:23px;">全选</button>
									<button type="button" class="btn btn-warning o-btn-del" style="margin-left:23px;">删除</button>
									<button type="button" class="btn btn-primary o-btn-export" style=" margin-left: 20px;">导出</button>
									</br>
							</div>

									
								  <thead>
								      <tr>     
								      		 <th>操作</th>	
								      	<!-- 	<th>序号</th> -->
								      		<th>项目名称</th>
								      		<th>活动类型</th>
								      	<!-- 	<th>详细内容</th> -->
								      		<th>活动开始时间</th>
								      		<th>活动结束时间</th>
								      		<th>活动是否通过</th>
								      		<th>遗留问题个数</th>
								      		<th>问题是否闭环</th>
								      	<!-- 	<th>问题闭环时间</th>
								      		<th>输出存放位置</th>
								      		<th>备注</th> -->
								      		
								      </tr>
								
								  </thead>
								  <tbody>
								  
								 <s:iterator value="#session.assurance_list" var="are">
									  <tr class="list">	 
									  		
									  		<td><input type="checkbox" class="o-checkbox" value="<s:property value="#are.id" />"></td>	
									    	<%-- <td><s:property value="#are.id"/></td>	 --%>						    	
									         <td><a href="#" data-toggle="modal" data-target="#modal-ZLBZ" assuranceid="<s:property value="#are.id" />" class="assurance-projectName" id="assurance-projectName-<s:property value="#are.id" />"><s:property value="#are.projectName" /></a></td> 
									    	<td><s:property value="#are.activeType"/></td>
									    	<%-- <td><s:property value="#are.content"/></td> --%>
									    	
									    	<td><s:date name="#are.startDate" format="yyyy:MM:dd   HH:mm:ss" /></td>
									    	<td><s:date name="#are.endDate" format="yyyy:MM:dd   HH:mm:ss"/></td>
									    	<td><s:property value="#are.activePass"/></td>
									    	<td><s:property value="#are.number"/></td>
									    	<td><s:property value="#are.problemClose"/></td>
									    	<%-- <td><s:property value="#are.closeDate"/></td>
									    	<td><s:property value="#are.address"/></td>
									    	<td><s:property value="#are.remark"/></td> --%>
									   
									    </tr>
									    	 </s:iterator>	
									</tbody>
								</table>
							</div>
							
							<div class="modal" id="modal-ZLBZ">
								<div class="modal-dialog">
									<div class="modal-content" style="height:100%;">
										<input type="hidden" value="" id="modal-assurance-id">
										<div class="modal-body">
											<form role="form" action="<%=path%>/assurance/Assurance_addassurance.action"  method="post">			
					                      		<div class="form-div">
					                      			<label>项目名称</label>
					                      			<div class="form-div-1" style="width:70%;float:left;margin-left:20px">
						                      			<!--select data-placeholder="Choose a Country..." class="select" tabindex="1" id="select-xmmc-2" style="display:block">
						                      				<option>111</option>
						                      				<option>222</option>
						                      				<option>333</option>
						                      			</select-->
						                      			<input class="form-control"  id="modal-assurance-projectName"  name="projectName">
					                      			</div>
					                      		</div>	
												<div class="form-div">
					                      			<div class="hd-div">
						                      			<label>活动类型</label>
						                      			<div>
							                      			<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="modal-assurance-activeType" name="activeType">
							                      				<option>产品检查</option>
							                      				<option>过程检查</option>
							                      				<option>召开评审会</option>
							                      			</select>
						                      			</div>
					                      			</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div>
					                      				<label>详细信息</label>
					                      				<div>
					                      					<textarea style="height:68px;" id="modal-assurance-content" name="content"></textarea>
					                      				</div>
					                      		</div>
					                      		</div>	
												<div class="form-div">
													<div class="hd-div">
							                      		<label>活动开始时间</label>
							                      		<div>
							                      			<div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" style="margin-left:0px;">
											                    <input class="form-control" size="16" id="date3" style="width:200px" type="text" value="" name="startDate" readonly>
											                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
																<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
										                	</div>
															<input type="hidden" id="dtp_input1" value="" name="startDate" /><br/>
							                      		</div>
							                      	</div>
							                    </div>	
												<div class="form-div">	
						                      		<div class="hd-div">
						                      			<label>活动结束时间</label>
						                      			<div>
							                      			<div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" style="margin-left:0px;">
											                    <input class="form-control" size="16" id="date4" style="width:200px" type="text" value="" name="endDate" readonly>
											                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
																<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
										                	</div>
															<input type="hidden" id="dtp_input2" value="" name="endDate" /><br/>
							                      		</div>
							                      	</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div class="hd-div">
					                      				<label>活动是否通过</label>
					                      				<div>
					                      					<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="modal-assurance-activePass" name="activePass">
							                      				<option>通过</option>
							                      				<option>不通过</option>
							                      			</select>
					                      				</div>
					                      			</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div class="hd-div">
					                      				<label>遗留问题个数</label>
					                      				<div>
					                      					<input class="form-control" id="modal-assurance-number" name="number">
					                      				</div>
					                      			</div>
					                      			<div class="hd-div">
					                      				<label>问题是否闭环</label>
					                      				<div>
					                      					<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="modal-assurance-problemClose" name="problemClose">
							                      				<option>已闭环</option>
							                      				<option>未闭环</option>
							                      			</select>
					                      				</div>
					                      			</div>
					                      		</div>	
												<div class="form-div">	
													<div class="hd-div">
							                      		<label>问题闭环时间</label>
							                      		<div >
							                      			<div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd" style="margin-left:0px;">
											                    <input class="form-control" size="16" style="width:150px" id="date2" type="text" value="" name="closeDate" readonly>
											                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
																<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										                	</div>
															<input type="hidden" id="dtp_input3" value="" name="closeDate" /><br/>
							                      		</div>
							                      	</div>	
													<div class="hd-div">	
							                      		<label>输出物存放位置</label>
							                      		<div >
							                      			<input class="form-control" id="modal-assurance-address" name="address">
							                      		</div>	
						                      		</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div>
					                      				<label>备注</label>
					                      				<div >
					                      					<textarea  id="modal-assurance-remark" name="remark"></textarea>
					                      				</div>
					                      			</div>
					                      		</div>
					                 
			                      		<div class="modal-footer">
				                      			<div style="position:absolute;left:40%;">
					                      			<button class="btn btn-default" id="" type="submit">新建</button>
					                      			
					                      			<button class="btn btn-default" data-dismiss="modal"  type="button" id="modal-submit-2" >更改</button> 
					                      			<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				                      			</div>
				                      	</div>
				                      	     	</form>
			                      		</div>
			                      	</div>
		                      	</div>
                      		</div>

                      		
                      		<div class="modal" id="modal-ZLBZ-1">
								<div class="modal-dialog">
									<div class="modal-content" style="height:100%;">
										<div class="modal-body">
											<form role="form" action="<%=path%>/assurance/Assurance_addassurance.action"  method="post">			
					                      		<div class="form-div">
					                      			<label>项目名称</label>
					                      			<div class="form-div-1" style="width:70%;float:left;margin-left:20px">
						                      			<!--select data-placeholder="Choose a Country..." class="select" tabindex="1" id="select-xmmc-2" style="display:block">
						                      				<option>111</option>
						                      				<option>222</option>
						                      				<option>333</option>
						                      			</select-->
						                      			<input class="form-control"  id="Name"  name="projectName">
					                      			</div>
					                      		</div>	
												<div class="form-div">
					                      			<div class="hd-div">
						                      			<label>活动类型</label>
						                      			<div>
							                      			<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="modal-management-activeType" name="activeType">
							                      				<option>产品检查</option>
							                      				<option>过程检查</option>
							                      				<option>召开评审会</option>
							                      			</select>
						                      			</div>
					                      			</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div>
					                      				<label>详细信息</label>
					                      				<div>
					                      					<textarea style="height:68px;" id="modal-assurance-content" name="content"></textarea>
					                      				</div>
					                      		</div>
					                      		</div>	
												<div class="form-div">
													<div class="hd-div">
							                      		<label>活动开始时间</label>
							                      		<div>
							                      			<div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" style="margin-left:0px;">
											                    <input class="form-control" size="16" id="startDate" style="width:200px" type="text" value="" name="startDate" readonly>
											                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
																<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
										                	</div>
															<input type="hidden" id="dtp_input1" value=""  name="startDate" /><br/>
							                      		</div>
							                      	</div>
							                    </div>	
												<div class="form-div">	
						                      		<div class="hd-div">
						                      			<label>活动结束时间</label>
						                      			<div>
							                      			<div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" style="margin-left:0px;">
											                    <input class="form-control" size="16" id="endDate" style="width:200px" type="text" value="" name="endDate" readonly>
											                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
																<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
										                	</div>
															<input type="hidden" id="dtp_input2" value="" name="endDate" /><br/>
							                      		</div>
							                      	</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div class="hd-div">
					                      				<label>活动是否通过</label>
					                      				<div>
					                      					<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="" name="activePass">
							                      				<option>通过</option>
							                      				<option>不通过</option>
							                      			</select>
					                      				</div>
					                      			</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div class="hd-div">
					                      				<label>遗留问题个数</label>
					                      				<div>
					                      					<input class="form-control" id="modal-assurance-number" name="number">
					                      				</div>
					                      			</div>
					                      			<div class="hd-div">
					                      				<label>问题是否闭环</label>
					                      				<div>
					                      					<select data-placeholder="Choose a Country..." class="select" tabindex="1" id="" name="problemClose">
							                      				<option>已闭环</option>
							                      				<option>未闭环</option>
							                      			</select>
					                      				</div>
					                      			</div>
					                      		</div>	
												<div class="form-div">	
													<div class="hd-div">
							                      		<label>问题闭环时间</label>
							                      		<div >
							                      			<div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd" style="margin-left:0px;">
											                    <input class="form-control" size="16" id="closeDate" style="width:150px" type="text" value="" name="closeDate" readonly>
											                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
																<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										                	</div>
															<input type="hidden" id="dtp_input3" value="" name="closeDate" /><br/>
							                      		</div>
							                      	</div>	
													<div class="hd-div">	
							                      		<label>输出物存放位置</label>
							                      		<div >
							                      			<input class="form-control" id="modal-assurance-address" name="address">
							                      		</div>	
						                      		</div>
					                      		</div>
					                      		<div class="form-div">
					                      			<div>
					                      				<label>备注</label>
					                      				<div >
					                      					<textarea  id="modal-assurance-remark" name="remark"></textarea>
					                      				</div>
					                      			</div>
					                      		</div>
					                 
			                      		<div class="modal-footer">
				                      			<div style="position:absolute;left:40%;">
					                      			<button class="btn btn-default" id="Save-2" type="sumbit">保存</button>
					                      			<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				                      			</div>
				                      	</div>
				                      			
				                      	     	</form>
				                      	     	
			                      		</div>
			                      	</div>
		                      	</div>
                      		</div>
                      		
                      		
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	
</body>


<script src="<%=path%>/plug-in/chosen_select/chosen.jquery.js" type="text/javascript"></script>
<script src="<%=path%>/plug-in/chosen_select/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
<script src="<%=path%>/plug-in/chosen_select/docsupport/init.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="<%=path%>/plug-in/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=path%>/plug-in/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript" charset="utf-8">

//左侧导航栏对应的跳转
$('.quality-list').on('click', function(event) {
	var quality_title = $(this).html();
	if (quality_title == '配置管理') {
        $('.nav-quality').eq(0).attr('class', 'nav-quality active');
        $('.nav-quality').eq(1).attr('class', 'nav-quality');
        $('.tab-pane').eq(0).attr('class', 'tab-pane active');
        $('.tab-pane').eq(1).attr('class', 'tab-pane');
	}
	if (quality_title == '质量保证') {
        $('.nav-quality').eq(0).attr('class', 'nav-quality');
        $('.nav-quality').eq(1).attr('class', 'nav-quality active');
        $('.tab-pane').eq(0).attr('class', 'tab-pane');
        $('.tab-pane').eq(1).attr('class', 'tab-pane active');
	}
});


$('.form_date').datetimepicker({
    language:  'ch',
    weekStart: 1,
    todayBtn:  1,
	autoclose: 1,
	//todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0,
	
    format : 'yyyy-mm-dd',
});
$('.form_datetime').datetimepicker({
    //language:  'fr',
    weekStart: 1,
    todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	forceParse: 0,
    showMeridian: 1,
    minView: 0,
    minuteStep:2,
    format : 'yyyy-mm-dd hh:ii:ss',
});
$('.form_time').datetimepicker({
    language:  'fr',
    weekStart: 1,
    todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 1,
	minView: 0,
	maxView: 1,
	forceParse: 0
});

/* //全选
$('.btn-info').on('click', function(event) {
	$("input[type=checkbox]").each(function(){
		$(this).attr('checked', !$(this).attr('checked'));
	});
});

$("#Added-1").on('click',function(){
    	$("#select-xmmc-1").hide();
    	$("#input-xmmc-1").show();
});
$("#Cancel-1").on('click',function(){
    	$("#select-xmmc-1").show();
    	$("#input-xmmc-1").hide();
}); 
$("#Added-2").on('click',function(){
    	$("#select-xmmc-2").hide();
    	$("#input-xmmc-2").show();
});
$("#Cancel-2").on('click',function(){
    	$("#select-xmmc-2").show();
    	$("#input-xmmc-2").hide();
});  */
    
//全选
$('.m-btn-info').on('click', function(event) {
	$("input[class=o-checkbox]").each(function(){
		$(this).attr('checked', !$(this).attr('checked'));
	});
});
$('.o-btn-info').on('click', function(event) {
	$("input[class=m-checkbox]").each(function(){
		$(this).attr('checked', !$(this).attr('checked'));
	});
});
    
    
//加载模态框的配置管理内容
$('.management-projectName').on('click', function(){
	var projectName = $(this).html();
	var id = $(this).attr('managementid');

    $.get("<%=path%>/management/AjaxManagement_querySingleManagement.action", {id:id}, function(data){
    	var management = eval('(' + data.management + ')');
    	$("#modal-management-id").val(management[0].id);
    	$("#modal-management-projectName").val(management[0].projectName);
        $("#modal-management-active").val(management[0].active);
        $("#modal-management-activeType").val(management[0].activeType);
        $("#modal-management-documentName").val(management[0].documentName);
        $("#modal-management-number").val(management[0].number);
        $("#modal-management-address").val(management[0].address);
        $("#modal-management-remark").val(management[0].remark);
        $("#date1").val(management[0].ajax_date);
    });
});   

//加载模态框的质量保证内容
$('.assurance-projectName').on('click', function(){
	var projectName = $(this).html();
	var id = $(this).attr('assuranceid');

    $.get("<%=path%>/assurance/AjaxAssurance_querySingleAssurance.action", {id:id}, function(data){
    	var assurance = eval('(' + data.assurance+ ')');console.log(assurance);
  		$("#modal-assurance-id").val(assurance[0].id);  
    	$("#modal-assurance-projectName").val(assurance[0].projectName);    
        $("#modal-assurance-activeType").val(assurance[0].activeType);
        $("#modal-assurance-content").val(assurance[0].content);
        $("#modal-assurance-address").val(assurance[0].address);
        $("#modal-assurance-number").val(assurance[0].number);
        $("#modal-assurance-remark").val(assurance[0].remark);
        $("#modal-assurance-problemClose").val(assurance[0].ajax_problemClose);
        $("#modal-assurance-activePass").val(assurance[0].ajax_activePass);
        $("#date2").val(assurance[0].ajax_date);
        $("#date3").val(assurance[0].ajax_startDate);
        $("#date4").val(assurance[0].ajax_EndDate);
    
    });
});   
 
 
//使用模态框编辑配置管理内容
$(function(){
	$('#modal-submit-1').on('click',function(){
		var id = $("#modal-management-id").val();
		var projectName = $("#modal-management-projectName").val();
		var active = $("#modal-management-active").val();
		var activeType = $("#modal-management-activeType").val();
		var documentName = $("#modal-management-documentName").val();
		var number = $("#modal-management-number").val();
		var address = $("#modal-management-address").val();
		var remark = $("#modal-management-remark").val();
		var date=$("#date1").val();
        if (projectName.length < 1 ) {
            layer.msg('名称不能为空');
            return false;
        }

		layer.confirm('你确定要修改此条记录吗？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/management/AjaxManagement_updateSingleManagement.action',
				type : 'POST',
				dataType : 'json',
				data : {
					id : id,
					projectName : projectName,
					active: active,
					activeType : activeType,
					documentName : documentName,
					number : number,
					address : address,
					remark : remark,
					date : date
				},
			})
			.done(function (data) {
				layer.msg(data.management);
				layer.close(deleteload);
				 setTimeout(function(){location.reload()},500); 
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法修改');
			});
		});
	});
});


//使用模态框编辑质量保证内容
$(function(){
	$('#modal-submit-2').on('click',function(){
		var id = $("#modal-assurance-id").val();
		var projectName = $("#modal-assurance-projectName").val();
		var activeType = $("#modal-assurance-activeType").val();
		var content = $("#modal-assurance-content").val();
		var number = $("#modal-assurance-number").val();
		var address = $("#modal-assurance-address").val();
		var remark = $("#modal-assurance-remark").val();
		var problemClose = $("#modal-assurance-problemClose").val();
		var activePass = $("#modal-assurance-activePass").val();
		var startDate=$("#date3").val();
		var endDate=$("#date4").val();
		var closeDate=$("#date2").val();
        if (projectName.length < 1 || startDate.length < 1) {
            layer.msg('名称不能为空');
            return false;
        }
		layer.confirm('你确定要修改此条记录吗？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/assurance/AjaxAssurance_updateSingleAssurance.action',
				type : 'POST',
				dataType : 'json',
				data : {
					id : id,
					projectName : projectName,
					activeType : activeType,
					content : content,
					number : number,
					address : address,
					remark : remark,
					startDate : startDate,
					endDate : endDate,
					closeDate : closeDate,
					problemClose : problemClose ,
					activePass : activePass
				},
			})
			.done(function (data) {
				layer.msg(data.assurance);
				layer.close(deleteload);	
				 setTimeout(function(){location.reload()},500); 
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法修改');
			});
		});
	});
});



//删除项目
$(".o-btn-del").on('click', function(event) {
	var id = '';
	$(".o-checkbox:checked").each(function(){
		id += $(this).val() + ',';
	});

	if (id.length) {
	    layer.confirm('确定删除项目？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/assurance/AjaxAssurance_deleteAssurance.action',
				type : 'POST',
				dataType : 'json',
				data : { id : id },
			})
			.done(function (data) {console.log(data);
				layer.close(deleteload);
				layer.msg(data.assurance);
                $(".o-checkbox:checked").each(function(){
                	$(this).parent().parent().remove();
                });
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法删除');
			});
		});
    } else {
    	layer.msg('选中要删除的项目');
    }
}); 

 $(".m-btn-del").on('click', function(event) {
	var id = '';
	$(".m-checkbox:checked").each(function(){
		id += $(this).val() + ',';
	});

	if (id.length) {
	    layer.confirm('确定删除项目吗？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/management/AjaxManagement_deleteManagement.action',
				type : 'POST',
				dataType : 'json',
				data : { id : id },
			})
			.done(function (data) {
				layer.close(deleteload);
				layer.msg(data.management);
                $(".m-checkbox:checked").each(function(){
                	$(this).parent().parent().remove();
                });
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法删除');
			});
		});
    } else {
    	layer.msg('选中要删除的项目');
    }
});


//导出x项目
 $(".m-btn-export").on('click', function(event) {
 	var id = '';
 	$(".m-checkbox:checked").each(function(){
 		id += $(this).val() + ',';
 	});
 	
 	if (id.length) {
 	    window.open("<%=path%>/management/Export_export.action?id=" + id);
     } else {
     	layer.msg('选中要导出的内容');
     }
 });

 $(".o-btn-export").on('click', function(event) {
 	var id = '';
 	$(".o-checkbox:checked").each(function(){
 		id += $(this).val() + ',';
 	});
 	
 	if (id.length) {
 	    window.open("<%=path%>/assurance/Export_export.action?id=" + id);
     } else {
     	layer.msg('选中要导出的内容');
     }
 });  
    
//页面跳转
 function redirect(str){
 	if (str == 'test') {
 		window.location.href = "<%=path%>/test/Test_queryTP.action";
 	}
 	if (str == 'weekly') {
 		window.location.href = "<%=path%>/weekly/Weekly_query.action";
 	}
 } 

 //简单的表单验证
$(function(){
	$('#Save-1').on('click', function(event) {
		var projectName = $("#projectName").val();
		var date = $("#date").val();
		if (projectName.length < 1 ) {
            layer.msg('名称不能为空');
            return false;
		}
		if (date.length < 1 ) {
            layer.msg('时间不能为空');
            return false;
		}
	});
});
 
$(function(){
	$('#Save-2').on('click', function(event) {
		var projectName = $("#Name").val();
		var startDate = $("#startDate").val();
		var endDate = $("#endDate").val();
		var closeDate = $("#closeDate").val();
		if (projectName.length < 1 ) {
            layer.msg('名称不能为空');
            return false;
		}
		if (startDate.length < 1 || endDate<1 || closeDate<1 ) {
            layer.msg('时间不能为空');
            return false;
		}
	});
});

//
$(function(){
	$('#tiajia').on('click', function(event) {
		var selObj = $("#test");
		var value=$("#text").val();
		var text=$("#text").val();
		selObj.append("<option value='"+value+"'>"+text+"</option>");
	});
});

 
 
    
/*var canvas = document.getElementById("barChart")
goBarChart(
        [[2007, 750], [2008, 425], [2009, 960], [2010, 700], [2011, 800], [2012, 975], [2013, 375], [2014, 775]],canvas     
);*/
</script>







</html>