<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>个人中心-项目测试</title>
<link rel="stylesheet" type="text/css" href="<%=path%>/plug-in/layui/css/layui.css" media="all">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap-theme.css">

<script src="<%=path%>/js/jquery-3.3.1.js" type="text/javascript" charset="utf-8"></script>
<script src="<%=path%>/js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>

    <div class="container">
		<div class="row clearfix">
			<div class="page-header">
				<h1 style="float: left;">
					<small>欢迎登陆</small> 产保中心 
				</h1>
				<div  style="float: right; margin: 20px; margin-bottom:0px;">
					<div >
						<div class="navbar-header">
							<a class="navbar-brand" href="">${sessionScope.loginUserName}</a>
						</div>
						<ul class="nav navbar-nav">
							<li><a href="<%=path%>/users/Users_modifyPassword.action">修改密码</a></li>
							<s:if test="#session.loginUserAuth == 1">
							<li><a href="<%=path%>/users/Users_admin.action">用户管理</a></li>
							</s:if>
							<li><a href="<%=path%>/users/Users_logout.action">退出</a></li>
						</ul>
					</div>
		        </div>
		        <div style="clear: both;"></div>
			</div>

			<div class="col-md-2 column">
				<div class="panel-group" id="panel-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a class="panel-title collapsed" href="#panel-element-1" data-toggle="collapse" data-parent="#panel-1" onclick="redirect('weekly')">
							    <span class="glyphicon glyphicon-edit"></span>&nbsp;周报
							</a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-1">
							<div class="panel-body">
								<ul>
									<li>
										<a href="./index.html">我的周报</a>
									</li>
									<li>
										<a href="./index.html">其他周报</a>
									</li>
									<li>
										<a href="./index.html">新建周报</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-2" data-toggle="collapse" data-parent="#panel-1">
							 	<span class="glyphicon glyphicon-music"></span>&nbsp;团建活动
							 </a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-2">
							<div class="panel-body">
								<ul>
									<li>
										<a href="./event.html">活动展示</a>
									</li>
									<li>
										<a href="./event.html">通知公告</a>
									</li>
									<li>
										<a href="./event.html">人员信息</a>
									</li>
									<li>
										<a href="./event.html">学习资料</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-3" data-toggle="collapse" data-parent="#panel-1" onclick="redirect('quality')">
							 	<span class="glyphicon glyphicon-tasks"></span>&nbsp;质量管理
							 </a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-3">
							<div class="panel-body">
								<ul>
									<li>
										<a href="./quality.html">质量项目</a>
									</li>
									<li>
										<a href="./quality.html">统计分析</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-4" data-toggle="collapse" data-parent="#panel-1">
							 	<span class="glyphicon glyphicon-th-list"></span>&nbsp;项目测试
							 </a>
						</div>
						<div class="panel-collapse collapse in" id="panel-element-4">
							<div class="panel-body">
								<ul>
									<li>
										<a href="#" class="protest-list">测试项目</a>
									</li>
									<li>
										<a href="#" class="protest-list">测试数据</a>
									</li>
									<li>
										<a href="#" class="protest-list">统计分析</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-10 column">
				<div class="tabbable" id="tabs-941325">
					<ul class="nav nav-tabs" id="report-nav">
					    <li class="active nav-protest">
							 <a href="#panel-41" data-toggle="tab">测试项目</a>
						</li>
						<li class="nav-protest">
							 <a href="#panel-42" data-toggle="tab">测试数据</a>
						</li>
						<li class="nav-protest">
							<a href="#panel-43" data-toggle="tab">统计分析</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="panel-41">
                            <table class="table table-hover table-striped">
                                <caption>
                                嘉兴产品保证中心项目总表
                                    <input class="btn btn-success btn-xs btn-add" type="button" value="新增" data-toggle="modal" data-target="#myModal" style="float:right;margin-right:30px;">
                                </caption>
								<thead>
									<tr>
										<th>名称</th>
										<th>类型</th>
										<th>负责人</th>
										<th>测试人员</th>
										<th>质量负责人</th>
										<th>进度</th>
										<th>预计验收时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
								    <s:iterator value="#session.tp_list" var="tpl">
									<tr>
										<td>
										    <s:property value="#tpl.title" />
										</td>
										<td><s:property value="#tpl.type" /></td>
										<td><s:property value="#tpl.leader" /></td>
										<td><s:property value="#tpl.member" /></td>
										<td><s:property value="#tpl.qa_leader" /></td>
										<td><s:property value="#tpl.progress" /></td>
										<td><s:date name="#tpl.reception_time" format="yyyy-MM-dd" /></td>
										<td>
										    <input class="btn btn-primary btn-xs btn-edit" type="button" value="编辑"  data-toggle="modal" data-target="#myModal" tpid="<s:property value="#tpl.id" />">
										    <input class="btn btn-warning btn-xs btn-del" type="button" value="删除" tpid="<s:property value="#tpl.id" />">
										</td>
									</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
						<div class="tab-pane" id="panel-42">
						    <form class="layui-form layui-form-pane" action="">
						        <div class="layui-form-item" style="margin-top:10px;">
								    <label class="layui-form-label">选择项目</label>
								    <div class="layui-input-block">
								        <select name="testproject" lay-filter="testproject">
									        <option value=""></option>
									        <s:iterator value="#session.tp_list" var="tpl">
									            <s:if test="#tpl.id == #session.tp_detail_request_id">
									                <option value="<s:property value="#tpl.id" />" selected><s:property value="#tpl.title" /></option>
									            </s:if>
									            <s:else>
									                <option value="<s:property value="#tpl.id" />"><s:property value="#tpl.title" /></option>
									            </s:else>
									        </s:iterator>
								        </select>
								    </div>
							    </div>
						    </form>
						    <input class="btn btn-success btn-xs btn-detail-add" type="button" value="新增" data-toggle="modal" data-target="#myModal-detail">
						    <s:iterator value="#session.tp_detail_list" var="tpdl">
							<table class="table table-hover table-striped">
                                <thead>
									<tr>
										<th colspan="2">标题</th>
										<th>开始时间</th>
										<th>结束时间</th>
										<th>测试人员</th>
										<th>工时</th>
									</tr>
							    </thead>
								<tbody>
                                    <tr>
                                        <td colspan="2"><s:property value="#tpdl.title" /></td>
                                        <td><s:date name="#tpdl.start_time" format="yyyy-MM-dd" /></td>
                                        <td><s:date name="#tpdl.end_time" format="yyyy-MM-dd" /></td>
                                        <td><s:property value="#tpdl.test_engineer" /></td>
                                        <td><s:property value="#tpdl.man_hour" /></td>
                                    </tr>
                                    <tr>
                                        <th>通过用例</th>
                                        <th>未通过用例</th>
                                        <th>阻塞用例</th>
                                        <th>轻微缺陷</th>
                                        <th>普通缺陷</th>
                                        <th>严重缺陷</th>
                                    </tr>
                                    <tr>
                                        <td><s:property value="#tpdl.data_param.pass" /></td>
                                        <td><s:property value="#tpdl.data_param.not_pass" /></td>
                                        <td><s:property value="#tpdl.data_param.block" /></td>
                                        <td><s:property value="#tpdl.data_param.s" /></td>
                                        <td><s:property value="#tpdl.data_param.n" /></td>
                                        <td><s:property value="#tpdl.data_param.h" /></td>
                                    </tr>
                                    <tr>
                                        <th>功能缺陷</th>
                                        <th>性能缺陷</th>
                                        <th>界面缺陷</th>
                                        <th>需求缺陷</th>
                                        <th>安全缺陷</th>
                                        <th>操作</th>
                                    </tr>
                                    <tr>
                                        <td><s:property value="#tpdl.data_param.feature" /></td>
                                        <td><s:property value="#tpdl.data_param.performance" /></td>
                                        <td><s:property value="#tpdl.data_param.interface" /></td>
                                        <td><s:property value="#tpdl.data_param.requirement" /></td>
                                        <td><s:property value="#tpdl.data_param.safety" /></td>
                                        <td>
                                            <input class="btn btn-primary btn-xs btn-detail-edit" type="button" value="编辑" data-toggle="modal" data-target="#myModal-detail" 
                                                style="margin-left:10px;" tpdid="<s:property value="#tpdl.id" />">
                                            <input class="btn btn-warning btn-xs btn-detail-del" type="button" value="删除"
                                                style="margin-left:10px;" tpdid="<s:property value="#tpdl.id" />" tpdtid="<s:property value="#tpdl.tid" />">
                                        </td>
                                    </tr>
								</tbody>
							</table>
							</s:iterator>
						</div>
						<script>
						window.onload = function () {
							var c1_y1 = ${sessionScope.statistic_tpd.case_pass};
							var c1_y2 = ${sessionScope.statistic_tpd.case_not_pass};
							var c1_y3 = ${sessionScope.statistic_tpd.case_block};
							var c2_y1 = ${sessionScope.statistic_tpd.bug_type_feature};
							var c2_y2 = ${sessionScope.statistic_tpd.bug_type_performance};
							var c2_y3 = ${sessionScope.statistic_tpd.bug_type_interface};
							var c2_y4 = ${sessionScope.statistic_tpd.bug_type_require};
							var c2_y5 = ${sessionScope.statistic_tpd.bug_type_safety};
							var c3_y1 = ${sessionScope.statistic_tpd.bug_degree_h};
							var c3_y2 = ${sessionScope.statistic_tpd.bug_degree_n};
							var c3_y3 = ${sessionScope.statistic_tpd.bug_degree_s};
							
							var chart1 = new CanvasJS.Chart("test-case-chart", {
								animationEnabled: true,
								exportEnabled: true,
								title:{
									text: "测试用例数据统计",
									horizontalAlign: "left"
								},
								data: [{
									type: "doughnut",
									startAngle: 60,
									//innerRadius: 60,
									indexLabelFontSize: 17,
									indexLabel: "{label} - #percent%",
									toolTipContent: "<b>{label}:</b> {y} (#percent%)",
									showInLegend: "true",
									legendText: "{label}：{y}",
									dataPoints: [
										{ y: c1_y1, label: "通过" },
										{ y: c1_y2, label: "未通过" },
										{ y: c1_y3, label: "阻塞" },
									]
								}]
							});
						    chart1.render();
						    var chart2 = new CanvasJS.Chart("bug-type-chart", {
								animationEnabled: true,
								exportEnabled: true,
								title:{
									text: "缺陷类型统计",
									horizontalAlign: "left"
								},
								data: [{
									type: "doughnut",
									startAngle: 60,
									//innerRadius: 60,
									indexLabelFontSize: 17,
									indexLabel: "{label} - #percent%",
									toolTipContent: "<b>{label}:</b> {y} (#percent%)",
									showInLegend: "true",
									legendText: "{label}：{y}",
									dataPoints: [
										{ y: c2_y1, label: "功能" },
										{ y: c2_y2, label: "性能" },
										{ y: c2_y3, label: "界面" },
										{ y: c2_y4, label: "需求" },
										{ y: c2_y5, label: "安全" },
									]
								}]
							});
						    chart2.render();
						    var chart3 = new CanvasJS.Chart("bug-degree-chart", {
								animationEnabled: true,
								exportEnabled: true,
								title:{
									text: "缺陷严重等级统计",
									horizontalAlign: "left"
								},
								data: [{
									type: "doughnut",
									startAngle: 60,
									//innerRadius: 60,
									indexLabelFontSize: 17,
									indexLabel: "{label} - #percent%",
									toolTipContent: "<b>{label}:</b> {y} (#percent%)",
									showInLegend: "true",
									legendText: "{label}：{y}",
									dataPoints: [
										{ y: c3_y1, label: "严重" },
										{ y: c3_y2, label: "普通" },
										{ y: c3_y3, label: "轻微" },
									]
								}]
							});
						    chart3.render();
						}
						</script>
						<div class="tab-pane" id="panel-43">
							<div id="test-case-chart" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
							<div id="bug-type-chart" style="height: 370px; max-width: 920px; margin: 30px auto;"></div>
							<div id="bug-degree-chart" style="height: 370px; max-width: 920px; margin: 30px auto;"></div>
                            <script src="<%=path%>/plug-in/canvasjs.min.js"></script>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<!-- 项目总表-概览-模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
                    <input type="text" value="" class="form-control" id="modal-tp-title" placeholder="请输入标题">
                    <input type="hidden" value="" id="modal-tp-tid">
				</div>
				<div class="modal-body">
				    <form class="layui-form layui-form-pane" action="">
				        <!-- div class="layui-form-item">
						    <label class="layui-form-label">项目类型</label>
						    <div class="layui-input-block">
						        <input type="text" name="title" autocomplete="off" placeholder="请输入类型" class="layui-input" value="" id="modal-tp-type">
						    </div>
						</div -->
						
						<div class="layui-form-item layui-form" lay-filter="modal-tp-filter">
						    <label class="layui-form-label">项目类型</label>
						    <div class="layui-input-block">
						        <select name="tp-type"  id="modal-tp-type">
							        <option value=""></option>
							        <option value="承接类">承接类</option>
							        <option value="自研类">自研类</option>
							        <option value="自研+外协">自研+外协</option>
						        </select>
						    </div>
						</div>
				    
				        <div class="layui-form-item layui-form" lay-filter="modal-tp-filter">
						    <label class="layui-form-label">测试负责人</label>
						    <div class="layui-input-block">
						        <!-- input type="text" name="title" autocomplete="off" placeholder="请输入项目负责人" class="layui-input" value="" id="modal-tp-leader" -->
						        <select lay-filter="modal-tp-filter" id="modal-tp-leader">
							        <option value=""></option>
							        <s:iterator value="#session.allUsers" var="users">
							            <option value="<s:property value="#users.username" />"><s:property value="#users.username" /></option>
							        </s:iterator>
						        </select>
						    </div>
						</div>
				    
						<div class="layui-form-item">
						    <label class="layui-form-label">测试人员</label>
						    <div class="layui-input-block">
						        <input type="text" name="title" autocomplete="off" placeholder="请输入测试人员，可以是多个" class="layui-input" value="" id="modal-tp-member">
						    </div>
					    </div>
				  
				       <div class="layui-form-item layui-form" lay-filter="modal-tp-filter">
						    <label class="layui-form-label">质量负责人</label>
						    <div class="layui-input-block">
						        <!-- input type="text" name="title" autocomplete="off" placeholder="请输入质量负责人" class="layui-input" value="" id="modal-tp-qa" -->
						        <select lay-filter="modal-tp-filter" id="modal-tp-qa">
							        <option value=""></option>
							        <s:iterator value="#session.allUsers" var="users">
							            <option value="<s:property value="#users.username" />"><s:property value="#users.username" /></option>
							        </s:iterator>
						        </select>
						    </div>
						</div>
						
						<div class="layui-form-item">
						    <label class="layui-form-label">项目进度</label>
						    <div class="layui-input-block">
						        <input type="text" name="title" autocomplete="off" placeholder="请输入项目进度" class="layui-input" value="" id="modal-tp-progress">
						    </div>
					    </div>
				  
						<div class="layui-form-item">
						    <div class="layui-inline">
						        <label class="layui-form-label">验收时间</label>
						        <div class="layui-input-block">
						            <input type="text" name="date" id="date1" autocomplete="off" class="layui-input" value="">
						        </div>
						    </div>
						</div>
				  
				    </form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						关闭
					</button>
					<button type="submit" class="btn btn-primary" data-dismiss="modal" id="modal-submit">
						提交
					</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal -->
	</div>
	
	<!-- 项目详情表，每一轮测试的详细数据 -->
	<div class="modal fade" id="myModal-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			    <form class="layui-form layui-form-pane" action="" id="tpd_modal_form">
					<div class="modal-header">
	                    <input type="text" value="" class="form-control" id="modal-tpd-title" placeholder="请输入标题，比如：XXX项目，第N轮XXX测试" name="tpd_title">
	                    <input type="hidden" value="" id="modal-tpd-id" name="tpd_id">
	                    <input type="hidden" value="${sessionScope.tp_detail_request_id}" id="modal-tpd-tid" name="tpd_tid">
					</div>
					<div class="modal-body">
						<div class="layui-form-item">
						    <div class="layui-inline">
						        <label class="layui-form-label">开始时间</label>
						        <div class="layui-input-block" style="width: 150px;">
						            <input type="text" name="tpd_s_time" id="date2" autocomplete="off" class="layui-input" value="">
						        </div>
						    </div>    
						    <div class="layui-inline">
						        <label class="layui-form-label">结束时间</label>
						        <div class="layui-input-inline" style="width: 150px;">
						            <input type="text" name="tpd_e_time" id="date3" autocomplete="off" class="layui-input" value="">
						        </div>
						    </div>
						</div>
						
						<div class="layui-form-item layui-form" lay-filter="modal-tpd-filter">
							<div class="layui-inline">
							    <label class="layui-form-label">测试工程师</label>
							    <div class="layui-input-block" style="width: 150px;">
							        <select id="modal-tpd-tester" name="tpd_tester">
								        <option value=""></option>
								        <s:iterator value="#session.allUsers" var="users">
								            <option value="<s:property value="#users.username" />"><s:property value="#users.username" /></option>
								        </s:iterator>
							        </select>
							    </div>
						    </div>
						    <div class="layui-inline">
							    <label class="layui-form-label">工时</label>
							    <div class="layui-input-inline" style="width: 150px;">
							        <input type="text" name="tpd_man_hour" autocomplete="off" placeholder="请输入工时" class="layui-input" value="" id="modal-tpd-man-hour">
							    </div>
						    </div>
						</div>
				  
				        <div class="layui-form-item">
				            <div class="layui-inline">
							    <label class="layui-form-label">用例统计</label>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_case_pass" autocomplete="off" placeholder="通过数量" title="通过" class="layui-input" value="" id="modal-tpd-case-pass">
							    </div>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_case_notpass" autocomplete="off" placeholder="未通过数量" title="未通过" class="layui-input" value="" id="modal-tpd-case-not-pass">
							    </div>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_case_block" autocomplete="off" placeholder="阻塞数量" title="阻塞" class="layui-input" value="" id="modal-tpd-case-block">
							    </div>
						    </div>
						</div>
						
						<div class="layui-form-item">
				            <div class="layui-inline">
							    <label class="layui-form-label">缺陷等级</label>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_bug_degree_s" autocomplete="off" placeholder="轻微数量" title="轻微" class="layui-input" value="" id="modal-tpd-bug-s">
							    </div>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_bug_degree_n" autocomplete="off" placeholder="普通数量" title="普通" class="layui-input" value="" id="modal-tpd-bug-n">
							    </div>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_bug_degree_h" autocomplete="off" placeholder="严重数量" title="严重" class="layui-input" value="" id="modal-tpd-bug-h">
							    </div>
						    </div>
						</div>
						
                        <div class="layui-form-item">
				            <div class="layui-inline">
							    <label class="layui-form-label">缺陷类型</label>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_bug_type_feature" autocomplete="off" placeholder="功能缺陷数量" title="功能" class="layui-input" value="" id="modal-tpd-bug-feature">
							    </div>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_bug_type_performance" autocomplete="off" placeholder="性能缺陷数量" title="性能" class="layui-input" value="" id="modal-tpd-bug-performance">
							    </div>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_bug_type_interface" autocomplete="off" placeholder="界面缺陷数量" title="界面" class="layui-input" value="" id="modal-tpd-bug-interface">
							    </div>
							</div>
				        </div>
				        
				        <div class="layui-form-item">
				            <div class="layui-inline">
							    <label class="layui-form-label">缺陷类型</label>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_bug_type_requirement" autocomplete="off" placeholder="需求缺陷数量" title="需求" class="layui-input" value="" id="modal-tpd-bug-requirement">
							    </div>
							    <div class="layui-input-inline" style="width: 135px;">
							        <input type="text" name="tpd_bug_type_safety" autocomplete="off" placeholder="安全缺陷数量" title="安全" class="layui-input" value="" id="modal-tpd-bug-safety">
							    </div>
							</div>
				        </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							关闭
						</button>
						<button type="submit" class="btn btn-primary" data-dismiss="modal" id="modal-submit-tpd">
							提交
						</button>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal -->
	</div>

</body>
<script src="<%=path%>/plug-in/layui/layui.js" charset="utf-8"></script>
<script src="<%=path%>/plug-in/layui/lay/modules/form.js" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
layui.use(['form', 'layedit', 'laydate'], function(){
	var form = layui.form
	,layer = layui.layer
	,layedit = layui.layedit
	,laydate = layui.laydate;
	
	//日期
	laydate.render({
	  elem: '#date1'
	});
	laydate.render({
	  elem: '#date2'
	});
	laydate.render({
	  elem: '#date3'
	});
	
	form.on('select(testproject)', function(data){
	    console.log(data.value); //得到被选中的值
	    $.ajax({
		url : '<%=path%>/test/AjaxTestDetail_queryAllTPD.action',
				type : 'POST',
				dataType : 'json',
				data : {id:data.value},
				success : function(data){
					if (data.tpd == 'ajax_query_tpd') {
						setTimeout(function(){location.reload()},500);
					}
				}
			})
	  });
	 
	});

//测试项目提交按钮
$(function(){
	$('#modal-submit').on('click',function(){
		var id = $("#modal-tp-tid").val();
		var title = $("#modal-tp-title").val();
		var type = $("#modal-tp-type").val();
		var leader = $("#modal-tp-leader").val();
		var qa_leader = $("#modal-tp-qa").val();
		var member = $("#modal-tp-member").val();
		var progress = $("#modal-tp-progress").val();
		var reception = $("#date1").val();

		layer.confirm('你确定要提交此条记录吗？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/test/AjaxTest_updateTP.action',
				type : 'POST',
				dataType : 'json',
				data : {
					id:id,
					title:title,
					type:type,
					leader:leader,
					member:member,
					qa_leader:qa_leader,
					progress:progress,
					reception:reception
				},
			})
			.done(function (data) {
				layer.close(deleteload);
				layer.msg(data.tp);
				setTimeout(function(){location.reload()},1500);
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法修改');
			});
		});
	});
});

$(function(){
	$('#modal-submit-tpd').on('click',function(){
		layer.confirm('你确定要提交此条记录吗？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/test/AjaxTestDetail_updateTPD.action',
				type : 'POST',
				data : $("#tpd_modal_form").serialize(),
			})
			.done(function (data) {
				layer.close(deleteload);
				layer.msg(data.tpd);
				setTimeout(function(){location.reload()},1500);
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法修改');
			});
		});
	});
});

//测试数据点击编辑按钮
$('.btn-detail-edit').on('click', function(){
	var id = $(this).attr('tpdid');

    $.get("<%=path%>/test/AjaxTestDetail_queryTPD.action", {id:id}, function(data){
    	var tpd = eval('(' + data.tpd + ')');
    	$("#modal-tpd-id").val(tpd[0].id);
    	$("#modal-tpd-tid").val(tpd[0].tid);
    	$("#modal-tpd-title").val(tpd[0].title);
    	$("#modal-tpd-tester").val(tpd[0].test_engineer);
    	$("#modal-tpd-man-hour").val(tpd[0].man_hour);
    	$("#date2").val(tpd[0].ajax_s_time);
    	$("#date3").val(tpd[0].ajax_e_time);
    	
    	$("#modal-tpd-case-pass").val(tpd[0].data_param.pass);
    	$("#modal-tpd-case-not-pass").val(tpd[0].data_param.not_pass);
    	$("#modal-tpd-case-block").val(tpd[0].data_param.block);
    	$("#modal-tpd-bug-s").val(tpd[0].data_param.s);
    	$("#modal-tpd-bug-n").val(tpd[0].data_param.n);
    	$("#modal-tpd-bug-h").val(tpd[0].data_param.h);
    	$("#modal-tpd-bug-feature").val(tpd[0].data_param.feature);
    	$("#modal-tpd-bug-performance").val(tpd[0].data_param.performance);
    	$("#modal-tpd-bug-interface").val(tpd[0].data_param.interface);
    	$("#modal-tpd-bug-requirement").val(tpd[0].data_param.requirement);
    	$("#modal-tpd-bug-safety").val(tpd[0].data_param.safety);
    	
    	layui.form.render('select', 'modal-tpd-filter');
    });
});

//点击新增按钮，清空数据
$('.btn-detail-add').on('click', function(){
   	$("#modal-tpd-id").val('');
   	$("#modal-tpd-title").val('');
   	$("#modal-tpd-tester").val('');
   	$("#date2").val('');
   	$("#date3").val('');
   	$("#modal-tpd-case-pass").val('');
   	$("#modal-tpd-case-not-pass").val('');
   	$("#modal-tpd-case-block").val('');
   	$("#modal-tpd-bug-s").val('');
   	$("#modal-tpd-bug-n").val('');
   	$("#modal-tpd-bug-h").val('');
   	$("#modal-tpd-bug-feature").val('');
   	$("#modal-tpd-bug-performance").val('');
   	$("#modal-tpd-bug-interface").val('');
   	$("#modal-tpd-bug-requirement").val('');
   	$("#modal-tpd-bug-safety").val('');
   	$("#modal-tpd-man-hour").val('');
   	
   	layui.form.render('select', 'modal-tpd-filter');
});

//删除按钮
$(function(){
	$('.btn-del').on('click',function(){
		var id = $(this).attr("tpid");

		layer.confirm('你确定要删除此条记录吗？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/test/AjaxTest_delTP.action',
				type : 'POST',
				dataType : 'json',
				data : {
					id:id
				},
			})
			.done(function (data) {
				layer.close(deleteload);
				layer.msg(data.tp);
				setTimeout(function(){location.reload()},1500);
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法修改');
			});
		});
	});
});

$(function(){
	$('.btn-detail-del').on('click',function(){
		var id = $(this).attr("tpdid");
        var tid = $(this).attr("tpdtid");
		layer.confirm('你确定要删除此条记录吗？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/test/AjaxTestDetail_delTPD.action',
				type : 'POST',
				dataType : 'json',
				data : {id:id,tid:tid},
			})
			.done(function (data) {
				layer.close(deleteload);
				layer.msg(data.tpd);
				setTimeout(function(){location.reload()},1500);
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法修改');
			});
		});
	});
});

//加载模态框的数据
$('.btn-edit').on('click', function(){
	var id = $(this).attr('tpid');

    $.get("<%=path%>/test/AjaxTest_queryTP.action", {id:id}, function(data){
    	var tp = eval('(' + data.tp + ')');
    	$("#modal-tp-tid").val(tp[0].id);
    	$("#modal-tp-title").val(tp[0].title);
    	
    	//$("#modal-tp-type").find("option[value='"+tp[0].type+"']").attr("selected",true);
    	//$("#modal-tp-leader").find("option[value='"+tp[0].leader+"']").attr("selected",true);
    	//$("#modal-tp-qa").find("option[value='"+tp[0].qa_leader+"']").attr("selected",true);
    	$("#modal-tp-type").val(tp[0].type);
    	$("#modal-tp-leader").val(tp[0].leader);
    	$("#modal-tp-qa").val(tp[0].qa_leader);
    	layui.form.render('select', 'modal-tp-filter');
    	
    	$("#modal-tp-leader").val(tp[0].leader);
    	$("#modal-tp-member").val(tp[0].member);
    	$("#modal-tp-progress").val(tp[0].progress);
    	$("#date1").val(tp[0].ajax_date);
    });
});
//点击新增按钮清空模态框内容
$('.btn-add').on('click', function(){
	$("#modal-tp-tid").val('');
	$("#modal-tp-title").val('');
	$("#modal-tp-type").val('');
	$("#modal-tp-leader").val('');
	$("#modal-tp-qa").val('');
	$("#modal-tp-member").val('');
	$("#modal-tp-progress").val('');
	$("#date1").val('');
});

//页面跳转
function redirect(str){
	if (str == 'weekly') {
		window.location.href = "<%=path%>/weekly/Weekly_query.action";
	}
	if (str == 'quality') {
		window.location.href = "<%=path%>/management/Management_query.action";
	}
}

//左侧导航栏对应的跳转
$('.protest-list').on('click', function(event) {
	var protest_title = $(this).html();
	if (protest_title == '测试项目') {
        $('.nav-protest').eq(0).attr('class', 'nav-protest active');
        $('.nav-protest').eq(1).attr('class', 'nav-protest');
        $('.nav-protest').eq(2).attr('class', 'nav-protest');
        $('.tab-pane').eq(0).attr('class', 'tab-pane active');
        $('.tab-pane').eq(1).attr('class', 'tab-pane');
        $('.tab-pane').eq(2).attr('class', 'tab-pane');
	}
	if (protest_title == '测试数据') {
        $('.nav-protest').eq(0).attr('class', 'nav-protest');
        $('.nav-protest').eq(1).attr('class', 'nav-protest active');
        $('.nav-protest').eq(2).attr('class', 'nav-protest');
        $('.tab-pane').eq(0).attr('class', 'tab-pane');
        $('.tab-pane').eq(1).attr('class', 'tab-pane active');
        $('.tab-pane').eq(2).attr('class', 'tab-pane');
	}
	if (protest_title == '统计分析') {
        $('.nav-protest').eq(0).attr('class', 'nav-protest');
        $('.nav-protest').eq(1).attr('class', 'nav-protest');
        $('.nav-protest').eq(2).attr('class', 'nav-protest active');
        $('.tab-pane').eq(0).attr('class', 'tab-pane');
        $('.tab-pane').eq(1).attr('class', 'tab-pane');
        $('.tab-pane').eq(2).attr('class', 'tab-pane active');
	}
});

//请求和页面的对应关系
$(function(){
	var tp_detail_request = ${sessionScope.tp_detail_request};
	if (tp_detail_request == 1) {
		$('.nav-protest').eq(0).attr('class', 'nav-protest');
        $('.nav-protest').eq(1).attr('class', 'nav-protest active');
        $('.nav-protest').eq(2).attr('class', 'nav-protest');
        $('.tab-pane').eq(0).attr('class', 'tab-pane');
        $('.tab-pane').eq(1).attr('class', 'tab-pane active');
        $('.tab-pane').eq(2).attr('class', 'tab-pane');
	}
});

</script>
</html>