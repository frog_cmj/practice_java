<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<title>个人中心-周报</title>
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/my.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/plug-in/chosen_select/chosen.css">
	<link href="<%=path%>/plug-in/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<script src="<%=path%>/js/jquery-3.3.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/layer/layer.js" charset="UTF-8"></script>
</head>

<body>
	<div class="container">
		<div class="row clearfix">
			<div class="page-header">
				<h1 style="float: left;">
					<small>欢迎登陆</small> 产保中心 
				</h1>
		        <div  style="float: right; margin: 20px; margin-bottom:0px;">
					<div >
						<div class="navbar-header">
							<a class="navbar-brand" href="">${sessionScope.loginUserName}</a>
						</div>
						<ul class="nav navbar-nav">
							<li><a href="<%=path%>/users/Users_modifyPassword.action">修改密码</a></li>
							<s:if test="#session.loginUserAuth == 1">
							<li><a href="<%=path%>/users/Users_admin.action">用户管理</a></li>
							</s:if>
							<li><a href="<%=path%>/users/Users_logout.action">退出</a></li>
						</ul>
					</div>
		        </div>
		        <div style="clear: both;"></div>
			</div>

			<div class="col-md-2 column">
				<div class="panel-group" id="panel-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a class="panel-title collapsed" href="#panel-element-1" data-toggle="collapse" data-parent="#panel-1">
							    <span class="glyphicon glyphicon-edit"></span>&nbsp;周报
							</a>
						</div>
						<div class="panel-collapse collapse in" id="panel-element-1">
							<div class="panel-body">
								<ul>
									<li>
										<a href="#" class="weekly-list">我的周报</a>
									</li>
									<li>
										<a href="#" class="weekly-list">其他周报</a>
									</li>
									<li>
										<a href="#" class="weekly-list">新建周报</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-2" data-toggle="collapse" data-parent="#panel-1">
							 	<span class="glyphicon glyphicon-music"></span>&nbsp;团建活动
							 </a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-2">
							<div class="panel-body">
								<ul>
									<li>
										<a href="./event.html">活动展示</a>
									</li>
									<li>
										<a href="./event.html">通知公告</a>
									</li>
									<li>
										<a href="./event.html">人员信息</a>
									</li>
									<li>
										<a href="./event.html">学习资料</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-3" data-toggle="collapse" data-parent="#panel-1" onclick="redirect('quality')">
							 	<span class="glyphicon glyphicon-tasks"></span>&nbsp;质量管理
							 </a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-3">
							<div class="panel-body">
								<ul>
									<li>
										<a href="./quality.html">质量项目</a>
									</li>
									<li>
										<a href="./quality.html">统计分析</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							 <a class="panel-title collapsed" href="#panel-element-4" data-toggle="collapse" data-parent="#panel-1" onclick="redirect('test')">
							 	<span class="glyphicon glyphicon-th-list"></span>&nbsp;项目测试
							 </a>
						</div>
						<div class="panel-collapse collapse" id="panel-element-4">
							<div class="panel-body">
								<ul>
									<li>
										<a href="<%=path%>/test/Test_queryTP.action">测试项目</a>
									</li>
									<li>
										<a href="./protest.html">测试数据</a>
									</li>
									<li>
										<a href="./protest.html">测试人员</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-10 column">
				<div class="tabbable" id="tabs-941325">
				    <% 
					int request_o = Integer.parseInt(session.getAttribute("request_o").toString());
					%>
					<ul class="nav nav-tabs" id="report-nav">
						<li class="nav-weekly <% if (request_o != 1) { %>active<% } %>">
							 <a href="#panel-11" data-toggle="tab">我的周报</a>
						</li>
						<li class="nav-weekly <% if (request_o == 1) { %>active<% } %>">
							 <a href="#panel-12" data-toggle="tab">其他周报</a>
						</li>
						<li class="nav-weekly">
							<a href="#panel-13" data-toggle="tab">新建周报</a>
						</li>
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane <% if (request_o != 1) { %>active<% } %>" id="panel-11">

	                        <s:iterator value="#session.weekly_list" var="wek">
                            <div>
								<h2>
									<input type="checkbox" class="m-checkbox" value="<s:property value="#wek.id" />">
									<a href="#" data-toggle="modal" data-target="#myModal" weeklyid="<s:property value="#wek.id" />" class="weekly-title" id="weekly-title-<s:property value="#wek.id" />"><s:property value="#wek.title" /></a>
									&nbsp;<small><s:date name="#wek.create_time" format="yyyy年MM月dd日" /></small>
									&nbsp;<s:if test="#wek.comment != null && #wek.comment != ''">
                                        <small style="color: red">有批注</small>
                                    </s:if>
								</h2>
								<p id="weekly-finished-<s:property value="#wek.id" />">
									<s:property value="#wek.finished" />
								</p>
	                        </div>
                            </s:iterator>
                            
                            <% int current_page = Integer.parseInt(session.getAttribute("current_page").toString());//个人中心当前页 %> 
                            <div>
								<ul class="pagination">
									<li>
										 <a href="<%if(current_page-1<=0){%><%}else{%><%=path%>/weekly/Weekly_query.action?page=<%=current_page-1%><%}%>">Prev</a>
									</li>
									<% for (int i = current_page - 2 > 0 ? current_page - 2 : 1; i < current_page + 3; i++) { %>
									<li <% if (i == current_page) { %>class="active"<%} %>>
										 <a href="<%=path%>/weekly/Weekly_query.action?page=<%=i%>"><%=i%></a>
									</li>
									<%} %>
									<li>
										 <a href="<%=path%>/weekly/Weekly_query.action?page=<%=current_page+1%>">Next</a>
									</li>
								</ul>
								<button type="button" class="btn btn-warning m-btn-del" style="float: right; margin: 20px;">删除</button>
								<button type="button" class="btn btn-primary m-btn-export" style="float: right; margin: 20px;">导出</button>
								<button type="button" class="btn btn-info m-btn-info" style="float: right; margin: 20px;">全选</button>
						    </div>
						</div>

						<div class="tab-pane <% if (request_o == 1) { %>active<% } %>" id="panel-12">
							<div class="list-search">
	                            <form role="form" action="<%=path%>/weekly/Weekly_query.action" method="post">
									<table>
										<colgroup>
										    <col style="width: 3%;">
											<col style="width: 20%;">
											<col style="width: 30%;">
											<col style="width: 30%;">
											<col style="width: 7%;">
										</colgroup>
										<tbody>
								    		<tr>
								    		    <td></td>
								    			<td>
								    				<select data-placeholder="search" class="chosen-select-deselect" tabindex="7" name="weekly_user_search_o">
											            <option value=""></option>
											            <s:iterator value="#session.allUsers" var="users">
											                <s:if test="#users.username == #session.weekly_user_search_o"><option selected></s:if>
											                <s:else><option></s:else>
												                <s:property value="#users.username" />
												            </option>
											            </s:iterator>
											        </select>
								    			</td>
								    			<td>
								    				<div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
									                    <input class="form-control" size="16" type="text" value="${sessionScope.weekly_start_time_o}" readonly>
									                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
														<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
									                </div>
													<input type="hidden" id="dtp_input1" value="${sessionScope.weekly_start_time_o}" name="weekly_start_time_o"/>
	                                            </td>
	                                            <td>
								    				<div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
									                    <input class="form-control" size="16" type="text" value="${sessionScope.weekly_end_time_o}" readonly>
									                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
														<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
									                </div>
													<input type="hidden" id="dtp_input2" value="${sessionScope.weekly_end_time_o}" name="weekly_end_time_o"/>
								    			</td>
								    			<td>
								    				<button class="btn btn-xs btn-primary" type="submit">
													<i class="icon-search"></i>
														search
													</button>
								    			</td>
								    		</tr>
								    	</tbody>
								    </table>
	                            </form>
							</div>
							
                            <s:iterator value="#session.weekly_list_o" var="wek_o">
							<div>
								<h2>
									<input type="checkbox" class="o-checkbox" value="<s:property value="#wek_o.id" />">
									<a href="#" data-toggle="modal" data-target="#myModal" class="weekly-title" weeklyid="<s:property value="#wek_o.id" />" id="weekly-title-<s:property value="#wek_o.id" />">
										<s:property value="#wek_o.title" />
									</a>&nbsp;<small><s:property value="#wek_o.username" /></small>
									&nbsp;<small><s:date name="#wek_o.create_time" format="yyyy年MM月dd日" /></small>
									&nbsp;<s:if test="#wek_o.comment != '' && #wek_o.comment != null">
                                        <small style="color: red">有批注</small>
                                    </s:if>
								</h2>
								<p id="weekly-finished-<s:property value="#wek_o.id" />">
									<s:property value="#wek_o.finished" />
								</p>
	                        </div>
	                        </s:iterator>

                            <% int current_page_o = Integer.parseInt(session.getAttribute("current_page_o").toString());//个人中心当前页 %> 
                            <div>
								<ul class="pagination">
									<li>
										 <a href="<%if(current_page_o-1<=0){%><%=path%>/weekly/Weekly_query.action?page_o=1<%}else{%><%=path%>/weekly/Weekly_query.action?page_o=<%=current_page_o-1%><%}%>">Prev</a>
									</li>
									<% for (int j = current_page_o - 2 > 0 ? current_page_o - 2 : 1; j < current_page_o + 3; j++) { %>
									<li <% if (j == current_page_o) { %>class="active"<%} %>>
										 <a href="<%=path%>/weekly/Weekly_query.action?page_o=<%=j%>"><%=j%></a>
									</li>
                                    <%} %>
									<li>
										 <a href="<%=path%>/weekly/Weekly_query.action?page_o=<%=current_page_o+1%>">Next</a>
									</li>
								</ul>
								<button type="button" class="btn btn-warning o-btn-del" style="float: right; margin: 20px;">删除</button>
								<button type="button" class="btn btn-primary o-btn-export" style="float: right; margin: 20px;">导出</button>
								<button type="button" class="btn btn-info o-btn-info" style="float: right; margin: 20px;">全选</button>
								<s:if test="#session.loginUserAuth == 1">
								    <button type="button" class="btn btn-danger" style="float: right; margin: 20px;" data-toggle="modal" data-target="#uncommitted-list-Modal">未提交名单</button>
								</s:if>
						    </div>
						</div>

						<div class="tab-pane" id="panel-13">
							<form role="form" action="<%=path%>/weekly/Weekly_addweekly.action" method="post">
								<div class="form-group">
									<label for="name">填写周报</label>
									<input type="text" class="form-control" name="title" placeholder="请输入标题（中文姓名+第N周）" value="">
								</div>
								<div class="form-group">
									<textarea class="form-control" rows="8" placeholder="本周已完成的工作内容（必填）" name="finished"></textarea>
								</div>
								<div class="form-group">
									<textarea class="form-control" rows="4" placeholder="还没完成的内容" name="notyet"></textarea>
								</div>
								<div class="form-group">
									<textarea class="form-control" rows="4" placeholder="未完成原因" name="reason"></textarea>
								</div>
								<div class="form-group">
									<textarea class="form-control" rows="4" placeholder="下周计划（必填）， 点击下面进度条录入完成百分比" name="nextweek"></textarea>
								</div>
								<div class="progress progress-striped active" id="progress-striped-new">
									<div class="progress-bar progress-bar-info" role="progressbar" style="width: 0%;"
									 id="progress-bar-new" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%
									</div>
									<input type="hidden" name="pct" value="0">
								</div>
								<button type="submit" class="btn btn-default" id="addnew-submit">提交</button>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- 模态框（Modal） -->
	<form role="form" id="modal-weekly-form">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
                        &nbsp;<label id="modal-weekly-user">user</label>
						<input type="hidden" id="modal-weekly-id">
						<input type="text" value="模态框（Modal）标题" class="form-control" id="modal-weekly-title">
					</div>
					<div class="modal-body">
						<div class="form-group">
							<textarea class="form-control" rows="8" id="modal-weekly-finished" placeholder="本周已完成的工作内容（必填）">周报内容</textarea>
						</div>
						<div class="form-group">
	                        <textarea class="form-control" rows="4" id="modal-weekly-notyet" placeholder="还没完成的内容"></textarea>
	                    </div>
	                    <div class="form-group">
							<textarea class="form-control" rows="4" id="modal-weekly-reason" placeholder="未完成原因"></textarea>
						</div>
						<div class="form-group">
							<textarea class="form-control" rows="4" id="modal-weekly-nextweek" placeholder="下周计划（必填） 点击下面进度条录入完成百分比"></textarea>
						</div>
						<br/>
						<div class="progress progress-striped active" id="progress-striped-pre">
							<div class="progress-bar progress-bar-info" role="progressbar" style="width: 20%;"
							id="progress-bar-pre" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%
							</div>
							<input type="hidden" id="modal-weekly-pct">
					    </div>
					</div>
					<div class="modal-footer">
					    <s:if test="#session.loginUserAuth == 1">
						    <div class="form-group">
								<textarea class="form-control" rows="2" id="modal-weekly-comment" placeholder="批注"></textarea>
							</div>
						</s:if>
						<s:else>
						    <p class="modal-comment-area" style="float: left"></p>
						</s:else>
						<button type="button" class="btn btn-default" data-dismiss="modal">
							关闭
						</button>
						<button type="submit" class="btn btn-primary" data-dismiss="modal" id="modal-submit">
							提交更改
						</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal -->
		</div>
    </form>

    <div class="modal fade" id="uncommitted-list-Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title" id="myModalLabel">未提交周报名单</h4>
	            </div>
	            <div class="modal-body">
                   <h3>以下名单为当前时间往前推7天，没有周报记录的用户</h3>
                   <p class="uncomminted-user-list"></p>
                </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal -->
	</div>
</body>

<script src="<%=path%>/plug-in/chosen_select/chosen.jquery.js" type="text/javascript"></script>
<script src="<%=path%>/plug-in/chosen_select/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
<script src="<%=path%>/plug-in/chosen_select/docsupport/init.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="<%=path%>/plug-in/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=path%>/plug-in/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript" charset="utf-8">
$(".chosen-container-single").css({width: '150px'});//下拉框宽度设置
$('.form_date').datetimepicker({
    language:  'ch',
    weekStart: 1,
    todayBtn:  1,
	autoclose: 1,
	//todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0,
    format : 'yyyy-mm-dd',
});

//查询未提交用户
$('.btn-danger').on('click', function(){
    $.get("<%=path%>/weekly/AjaxWeekly_uncommittedUser.action", {}, function(data){
    	var weekly = eval('(' + data.weekly + ')');
        var namelist = "";
        for (var name in weekly) {
        	namelist += weekly[name] + "<br/>";
        }
        $(".uncomminted-user-list").html(namelist);
    });
});

//删除周报
$(".m-btn-del").on('click', function(event) {
	var id = '';
	$(".m-checkbox:checked").each(function(){
		id += $(this).val() + ',';
	});

	if (id.length) {
	    layer.confirm('确定删除周报？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/weekly/AjaxWeekly_delWeekly.action',
				type : 'POST',
				dataType : 'json',
				data : { id : id },
			})
			.done(function (data) {
				layer.close(deleteload);
				layer.msg(data.weekly);
                $(".m-checkbox:checked").each(function(){
                	$(this).parent().parent().remove();
                });
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法删除');
			});
		});
    } else {
    	layer.msg('选中要删除的周报');
    }
});

$(".o-btn-del").on('click', function(event) {
	var id = '';
	$(".o-checkbox:checked").each(function(){
		id += $(this).val() + ',';
	});

	if (id.length) {
	    layer.confirm('确定删除周报？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/weekly/AjaxWeekly_delWeekly.action',
				type : 'POST',
				dataType : 'json',
				data : { id : id },
			})
			.done(function (data) {
				layer.close(deleteload);
				layer.msg(data.weekly);
                $(".o-checkbox:checked").each(function(){
                	$(this).parent().parent().remove();
                });
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法删除');
			});
		});
    } else {
    	layer.msg('选中要删除的周报');
    }
});

//导出周报
$(".m-btn-export").on('click', function(event) {
	var id = '';
	$(".m-checkbox:checked").each(function(){
		id += $(this).val() + ',';
	});
	
	if (id.length) {
	    window.open("<%=path%>/weekly/Export_export.action?id=" + id);
    } else {
    	layer.msg('选中要导出的内容');
    }
});

$(".o-btn-export").on('click', function(event) {
	var id = '';
	$(".o-checkbox:checked").each(function(){
		id += $(this).val() + ',';
	});
	
	if (id.length) {
	    window.open("<%=path%>/weekly/Export_export.action?id=" + id);
    } else {
    	layer.msg('选中要导出的内容');
    }
});

//新建周报内容的简单过滤
$(function(){
	$('#addnew-submit').on('click', function(event) {
		var title = $("input[name=title]").val();
		var finished = $("input[name=finished]").val();
		if (title.length < 1 ||finished.length < 1) {
            layer.msg('标题或正文不能为空');
            return false;
		}
	});
});

//使用模态框编辑周报内容
$(function(){
	$('#modal-submit').on('click',function(){
		var id = $("#modal-weekly-id").val();
		var title = $("#modal-weekly-title").val();
		var finished = $("#modal-weekly-finished").val();
		var notyet = $("#modal-weekly-notyet").val();
		var reason = $("#modal-weekly-reason").val();
		var nextweek = $("#modal-weekly-nextweek").val();

		<s:if test="#session.loginUserAuth == 1">
		var comment = $("#modal-weekly-comment").val()
	    </s:if>
	    <s:else>
	    comment = $(".modal-comment-area").html().replace("批注：","")
	    </s:else>
		
		var pct = $("#modal-weekly-pct").val();
		
        if (title.length < 1 || finished.length < 1) {
            layer.msg('标题或正文不能为空');
            return false;
        }

		layer.confirm('你确定要修改此条记录吗？', {
			btn : ['确定', '取消']
		}, function (index, layero) {
			layer.close(index);
			var deleteload = layer.load(1);
			$.ajax({
				url : '<%=path%>/weekly/AjaxWeekly_updateSingleWeekly.action',
				type : 'POST',
				dataType : 'json',
				data : {
					id : id,
					title : title,
					finished : finished,
					notyet : notyet,
					reason : reason,
					nextweek : nextweek,
					comment, comment,
					pct : pct
				},
			})
			.done(function (data) {
				layer.close(deleteload);
				layer.msg(data.weekly);
                $("#weekly-title-"+id).html(title);
                $("#weekly-finished-"+id).html(finished);
			})
			.fail(function (data) {
				layer.close(deleteload);
				layer.alert('发生内部错误，暂时无法修改');
			});
		});
	});
});

//加载模态框的周报内容
$('.weekly-title').on('click', function(){
	var title = $(this).html();
	var id = $(this).attr('weeklyid');

    $.get("<%=path%>/weekly/AjaxWeekly_querySingleWeekly.action", {id:id}, function(data){
    	var weekly = eval('(' + data.weekly + ')');
    	$("#modal-weekly-user").html(weekly[0].username);
    	$("#modal-weekly-id").val(weekly[0].id);
    	$("#modal-weekly-title").val(weekly[0].title);
        $("#modal-weekly-finished").val(weekly[0].finished);
        $("#modal-weekly-notyet").val(weekly[0].notyet);
        $("#modal-weekly-reason").val(weekly[0].reason);
        $("#modal-weekly-nextweek").val(weekly[0].nextweek);
        <s:if test="#session.loginUserAuth == 1">
            $("#modal-weekly-comment").val(weekly[0].comment);
        </s:if>
        <s:else>
            $(".modal-comment-area").html("批注："+weekly[0].comment);
	    </s:else>
        $("#modal-weekly-pct").val(weekly[0].pct);
        $("#progress-bar-pre").html(weekly[0].pct+"%");
        $("#progress-bar-pre").attr('aria-valuenow', weekly[0].pct);
        $("#progress-bar-pre").css('width', weekly[0].pct + "%");
    });
});

//左侧导航栏对应的跳转
$('.weekly-list').on('click', function(event) {
	var weekly_title = $(this).html();
	if (weekly_title == '我的周报') {
        $('.nav-weekly').eq(0).attr('class', 'nav-weekly active');
        $('.nav-weekly').eq(1).attr('class', 'nav-weekly');
        $('.nav-weekly').eq(2).attr('class', 'nav-weekly');
        $('.tab-pane').eq(0).attr('class', 'tab-pane active');
        $('.tab-pane').eq(1).attr('class', 'tab-pane');
        $('.tab-pane').eq(2).attr('class', 'tab-pane');
	}
	if (weekly_title == '其他周报') {
        $('.nav-weekly').eq(0).attr('class', 'nav-weekly');
        $('.nav-weekly').eq(1).attr('class', 'nav-weekly active');
        $('.nav-weekly').eq(2).attr('class', 'nav-weekly');
        $('.tab-pane').eq(0).attr('class', 'tab-pane');
        $('.tab-pane').eq(1).attr('class', 'tab-pane active');
        $('.tab-pane').eq(2).attr('class', 'tab-pane');
	}
	if (weekly_title == '新建周报') {
        $('.nav-weekly').eq(0).attr('class', 'nav-weekly');
        $('.nav-weekly').eq(1).attr('class', 'nav-weekly');
        $('.nav-weekly').eq(2).attr('class', 'nav-weekly active');
        $('.tab-pane').eq(0).attr('class', 'tab-pane');
        $('.tab-pane').eq(1).attr('class', 'tab-pane');
        $('.tab-pane').eq(2).attr('class', 'tab-pane active');
	}
});

//进度条点击事件
$(function(){
    var left = 0;
    //新建周报进度条
    $('#progress-striped-new').mousedown(function(e) {
        ox = e.pageX - left;
        var lbar = $('#progress-striped-new').offset().left;
        var wbar = $('#progress-striped-new').width();
        var per = parseInt((ox-lbar)/wbar*100);
        per == 99 ? (per = 100) : '';//无法完全达到100
        $('#progress-bar-new').attr({
        	'aria-valuenow' : per,
        	'style' : 'width:' + per + '%'
        }).html(per+'%');
        $("input[name='pct']").val(per);
    });
    //模态框进度条
    $('#progress-striped-pre').mousedown(function(e) {
        ox = e.pageX - left;
        var lbar = $('#progress-striped-pre').offset().left;
        var wbar = $('#progress-striped-pre').width();
        var per = parseInt((ox-lbar)/wbar*100);
        per == 99 ? (per = 100) : '';
        $('#progress-bar-pre').attr({
        	'aria-valuenow' : per,
        	'style' : 'width:' + per + '%'
        }).html(per+'%');
        $("#modal-weekly-pct").val(per);
    });
});

//全选
$('.m-btn-info').on('click', function(event) {
	$("input[class=m-checkbox]").each(function(){
		$(this).attr('checked', !$(this).attr('checked'));
	});
});
$('.o-btn-info').on('click', function(event) {
	$("input[class=o-checkbox]").each(function(){
		$(this).attr('checked', !$(this).attr('checked'));
	});
});

//页面跳转
function redirect(str){
	if (str == 'test') {
		window.location.href = "<%=path%>/test/Test_queryTP.action";
	}
	if (str == 'quality') {
		window.location.href = "<%=path%>/management/Management_query.action";
	}
}
</script>
</html>