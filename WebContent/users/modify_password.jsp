<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<title>修改密码</title>
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap-theme.css">
	<script src="<%=path%>/js/jquery-3.3.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/layer/layer.js" charset="UTF-8"></script>
</head>

<body>
	<div class="container">
		<div class="row clearfix">
			<div class="page-header">
				<h1 style="float: left;">
					<small>欢迎登陆</small> 产保中心 
				</h1>
				<!--div class="btn-group" style="float: left; margin: 20px;">
					<button type="button" class="btn btn-default dropdown-toggle" 
						data-toggle="dropdown">
					${sessionScope.loginUserName}
					<span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li>
							 <a href="<%=path%>/users/Users_modifyPassword.action">修改密码</a>
						</li>
						<li>
							 <a href="">用户管理</a>
						</li>
						<li class="divider">
						</li>
						<li>
							 <a href="<%=path%>/users/Users_logout.action">退出</a>
						</li>
					</ul>
		        </div-->
		        <div  style="float: right; margin: 20px; margin-bottom:0px;">
					<div >
						<div class="navbar-header">
							<a class="navbar-brand" href="">${sessionScope.loginUserName}</a>
						</div>
						<ul class="nav navbar-nav">
							<li><a href="<%=path%>/users/Users_modifyPassword.action">修改密码</a></li>
							<s:if test="#session.loginUserAuth == 1">
							<li><a href="<%=path%>/users/Users_admin.action">用户管理</a></li>
							</s:if>
							<li><a href="<%=path%>/users/Users_logout.action">退出</a></li>
						</ul>
					</div>
		        </div>
		        <div style="clear: both;"></div>
			</div>

			<div class="col-md-12 column">
				<ol class="breadcrumb">
				    <li><a href="#">${sessionScope.loginUserName}</a></li>
				    <li><a href="<%=path%>/weekly/Weekly_query.action">个人中心</a></li>
				    <li class="active">修改密码</li>
				</ol>
				<div class="form-group">
					<label for="name">输入新密码</label>
                    <input type="password" class="form-control" id="new-password" placeholder="请输入新密码">
                    <br>
                    <label for="name">确认新密码</label>
                    <input type="password" class="form-control" id="confirm-password" placeholder="确认新密码">
                    <br>
                    <div class="alert alert-danger" style="display: none">两次输入的密码不一致或者密码过短，请重新输入</div>
                    <div class="alert alert-success" style="display: none">修改密码成功</div>
                    <button type="submit" class="btn btn-default btn-default-pwd">提交</button>
				</div>
		    </div>

        </div>
    </div>

    <script type="text/javascript" charset="utf-8">
    	$('.btn-default-pwd').on('click', function(event) {
    		var npwd = $("#new-password").val();
    		var cpwd = $("#confirm-password").val();

    		if (npwd != cpwd || npwd.length < 5) {
                $(".alert-danger").css({ display: 'block' });
                $(".alert-success").css({ display: 'none' });
    		} else {
    			$.post(
                    "<%=path%>/users/AjaxUser_doModifyPassword.action",
                    {pwd:npwd},
                    function(e){console.log(e);
                    	if (e.uRes == 'success') {
                            $(".alert-danger").css({ display: 'none' });
			                $(".alert-success").css({ display: 'block' });
                    	}
                    }
    			);
    		}
            $("#new-password").val('');
            $("#confirm-password").val('');
    	});
    </script>
</body>