<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<title>用户管理</title>
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap-theme.css">
	<script src="<%=path%>/js/jquery-3.3.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/layer/layer.js" charset="UTF-8"></script>
</head>

<body>
	<div class="container">
		<div class="row clearfix">
			<div class="page-header">
				<h1 style="float: left;">
					<small>欢迎登陆</small> 产保中心 
				</h1>
				<div  style="float: right; margin: 20px; margin-bottom:0px;">
					<div >
						<div class="navbar-header">
							<a class="navbar-brand" href="">${sessionScope.loginUserName}</a>
						</div>
						<ul class="nav navbar-nav">
							<li><a href="<%=path%>/users/Users_modifyPassword.action">修改密码</a></li>
							<s:if test="#session.loginUserAuth == 1">
							<li><a href="">用户管理</a></li>
							</s:if>
							<li><a href="<%=path%>/users/Users_logout.action">退出</a></li>
						</ul>
					</div>
		        </div>
		        <div style="clear: both;"></div>
			</div>

            <ol class="breadcrumb">
			    <li><a href="#">admin</a></li>
			    <li><a href="<%=path%>/weekly/Weekly_query.action">个人中心</a></li>
			    <li class="active">用户管理</li>
			</ol>

			<div class="col-md-6 column">
				<table class="table table-striped">
					<caption><span class="glyphicon glyphicon-user">&nbsp;用户列表</span></caption>
					<thead>
					<tr>
						<th>用户名</th>
						<th>权限</th>
						<th>操作</th>
					</tr>
                    <s:iterator value="#session.allUsers" var="users">
                    <tr <s:if test="#users.auth == 1">class="active"</s:if>>
                    	<td><s:property value="#users.username" /></td>
                    	<td>
                    	    <s:if test="#users.auth == 1">是</s:if>
                    	    <s:else>否</s:else>
                    	</td>
                    	<td>
                    		<input class="btn btn-default btn-xs bth-del-user" type="button" value="删除" uid="<s:property value="#users.uid" />">
                    	</td>
                    </tr>
				    </s:iterator>
					</thead>
				</table>
		    </div>

		    <div class="col-md-6 column">
				<span class="glyphicon glyphicon-plus-sign">&nbsp;添加用户</span>
				<form role="form" action="<%=path%>/users/Users_addUser.action" method="post" onsubmit="return checkdata()">
					<div class="form-group">
						<br>
						<input type="text" class="form-control" name="username" placeholder="请输入用户名">
						<br>
						<input type="text" class="form-control" name="password" placeholder="请输入密码">
					</div>
					<div class="alert alert-danger" style="display: none">请检查输入的账号名或密码</div>
					<s:if test="#session.addUser == 1"><div class="alert alert-success" style="display: block">添加用户成功</div></s:if>
					<s:if test="#session.addUser == 2"><div class="alert alert-danger" style="display: block">添加用户失败</div></s:if>
					<div class="checkbox">
						<label>
						    <input type="checkbox" name="auth" value="1">权限
						</label>
					</div>
					<button type="submit" class="btn btn-default">提交</button>
				</form>
		    </div>

        </div>
    </div>

    <script type="text/javascript" charset="utf-8">
		$(function(){
			$('.bth-del-user').on('click',function(){
				var uid = $(this).attr('uid');
				
				layer.confirm('你确定要删除此用户吗？', {
					btn : ['确定', '取消']
				}, function (index, layero) {
					layer.close(index);
					var deleteload = layer.load(1);
					$.ajax({
						url : '<%=path%>/users/AjaxUser_delUser.action',
						type : 'POST',
						dataType : 'json',
						data : {
							uid : uid,
						},
					})
					.done(function (data) {
						layer.close(deleteload);
						layer.msg(data.uRes);
                        $("input[uid="+uid+"]").parent().parent().remove();
					})
					.fail(function (data) {
						layer.close(deleteload);
						layer.alert('发生内部错误，暂时无法修改');
					});
				});
			});
		});

    	//检查添加用户的信息
    	function checkdata() {
    		var username = $("input[name=username]").val();
    		var password = $("input[name=password]").val();

            if ( username.length < 3 && password.length < 5) {
                $(".alert-danger").css({ display: 'block' });
                $(".alert-success").css({ display: 'none' });
                return false;
            }
            return true;
    	}
    </script>
</body>