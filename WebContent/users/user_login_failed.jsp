<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<title>登陆</title>
	<link href="<%=path%>/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/bootstrap-theme.css">
	<script src="<%=path%>/js/jquery-3.3.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=path%>/js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
	<div class="container">
	<div class="row clearfix">
		<div class="col-md-4 column"></div>
		<div class="col-md-4 column" style="margin-top: 250px;">
			<form role="form" action="<%=path%>/users/Users_login.action" method="post">
				<h2>产品保证中心</h2>
				<div class="form-group">
					 <label for="account-field">Account</label><input class="form-control" id="account-field" type="text" name="username"/>
				</div>
				<div class="form-group">
					 <label for="pwd-field">Password</label><input class="form-control" id="pwd-field" type="password" name="password"/>
				</div>
				<div class="checkbox">
					 <label><input type="checkbox" />remember me</label>
				</div>
				<div class="alert alert-danger" style="display: none">账号或密码不能为空</div>
				<div class="alert alert-danger" style="display: block">账号或密码不正确</div>
				<button class="btn btn-default" type="submit">Sign in</button>
			</form>
		</div>
		<div class="col-md-4 column"></div>
	</div>
</div>
</body>

<script>
	//简单的表单验证
    $('form').submit(function(e){
        var account = $('#account-field').val();
        var pwd = $('#pwd-field').val();
        if (!account.length || !pwd.length) {
            $('.alert-danger').eq(0).css('display', 'block');
            $('.alert-danger').eq(1).css('display', 'none');
            return false;
        }
    });
</script>
</html>