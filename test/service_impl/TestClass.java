package service_impl;

import java.io.IOException;
import java.util.*;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestClass {

	@Test
	public void testMap()
	{
		Map<String, String> testmap = new HashMap<String, String>();
		testmap.put("t1", "test111");
		testmap.put("t2", "test222");
		System.out.println(testmap);
	}
	
	private static ObjectMapper MAPPER = new ObjectMapper();
	@Test
	public void testJsonToMap() throws JsonParseException, JsonMappingException, IOException
	{
		String json = "{\"name\":\"1\", \"age\":29}";
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		map = MAPPER.readValue(json, new TypeReference<Map<String, Integer>>(){});
		
		System.out.println(map.get("name"));
	}
}
