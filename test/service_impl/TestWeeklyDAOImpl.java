package service_impl;

//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.Test;

import entity.Weekly;
import net.sf.json.JSONArray;
import service.WeeklyDAO;
import service.impl.WeeklyDAOImpl;

public class TestWeeklyDAOImpl {
	
	@Test
	public void testQueryAllWeekly()
	{
		WeeklyDAO wdao = new WeeklyDAOImpl();
		List<Weekly> list = wdao.queryAllWeekly(1,1);
		
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
	
	@Test
	public void testAjaxQueryWeekly()
	{
		WeeklyDAO wdao = new WeeklyDAOImpl();
		List<Weekly> list = wdao.queryWeeklyById(1);
		
		System.out.println(list);
	}
	
	@Test
	public void testJson()
	{
	    String testJson = "";
		
	    WeeklyDAO wdao = new WeeklyDAOImpl();
		List<Weekly> list = wdao.queryWeeklyById(51);
		list.get(0).setCreate_time(null);
		testJson = JSONArray.fromObject(list).toString();
		
		System.out.println("test:"+testJson);
	}
	
	@Test
	public void stringToDate()
	{
		Date d = null;
		System.out.println(d);
		/*
		String string = "2018-06-05";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			System.out.println(sdf.parse(string));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}
	
	@Test
	public void testPlus()
	{
		int i = 1;
		int j = i++;
		System.out.println(i);
		System.out.println(j);
	}
	
	@Test
	public void updateWeekly()
	{
		Map<String, String> name_map = new HashMap<String, String>();
		name_map.put("chenminjie", "陈敏杰");
		name_map.put("yangzixi", "杨紫熙");
		name_map.put("gongguifeng", "龚桂峰");
		name_map.put("wujiasong", "吴家松");
		name_map.put("wangshuwen", "王舒文");
		name_map.put("heliujie", "贺刘杰");
		name_map.put("gaoxianchi", "高咸池");
		name_map.put("lujidong", "陆吉东");
		name_map.put("jinshihao", "金诗壕");
		name_map.put("tianmingyu", "田明宇");
		name_map.put("zhonglingjun", "钟灵君");
		name_map.put("jinyangyu", "金杨煜");
		name_map.put("baijun", "白军");
		name_map.put("wubin", "吴彬");
		name_map.put("yangting", "杨婷");
		name_map.put("hanlulu", "韩路路");
		name_map.put("liaoxiaomei", "廖晓梅");
		name_map.put("huanglijuan", "黄丽娟");
		
		Map<String, Integer> name_count = new HashMap<String, Integer>();
		name_count.put("chenminjie", 1);
		name_count.put("yangzixi", 1);
		name_count.put("gongguifeng", 1);
		name_count.put("wujiasong", 1);
		name_count.put("wangshuwen", 1);
		name_count.put("heliujie", 1);
		name_count.put("gaoxianchi", 1);
		name_count.put("lujidong", 1);
		name_count.put("jinshihao", 1);
		name_count.put("tianmingyu", 1);
		name_count.put("zhonglingjun", 1);
		name_count.put("jinyangyu", 1);
		name_count.put("baijun", 1);
		name_count.put("wubin", 1);
		name_count.put("yangting", 1);
		name_count.put("hanlulu", 1);
		name_count.put("liaoxiaomei", 1);
		name_count.put("huanglijuan", 1);
		
		String username = "";
		List<Weekly> wk = null;
		
		//创建配置对象
		Configuration config = new Configuration().configure();
		//创建服务注册对象
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
		//创建sessionFactory
		SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);
		//创建session对象
		Session session = sessionFactory.getCurrentSession();
		
		Transaction tx = session.beginTransaction();
		
		String hql1 = "from Weekly";
		
		Query query = session.createQuery(hql1);
		List<Weekly> list = query.list();
		
		for (int i = 0; i<list.size(); i++) {
			list.get(i).setTitle(name_map.get(list.get(i).getUsername()) + "-第" + name_count.get(list.get(i).getUsername()) + "周");
			name_count.put(list.get(i).getUsername(), name_count.get(list.get(i).getUsername())+1);
			//System.out.println("test:"+i);
			session.update(list.get(i));
		}
		
		
		tx.commit();
		sessionFactory.close();
		
	}
	
}
