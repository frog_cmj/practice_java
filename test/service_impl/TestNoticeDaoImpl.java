package service_impl;

import entity.Notice;
import org.junit.Test;
import service.NoticesDao;
import service.impl.NoticesDaoImpl;

import java.util.Date;
import java.util.List;


public class TestNoticeDaoImpl {

    //测试查询
    @Test
    public void testqueryAllNotice(){
        NoticesDao ndao=new NoticesDaoImpl();
        List<Notice> list=ndao.queryAllNotices();



    }

    //测试查询某个通知
    @Test
    public void testqueryNotice(){
        NoticesDao ndao=new NoticesDaoImpl();
        Notice notice=(Notice) ndao.queryNoticeById(Notice.class,1);
        System.out.println("查询结果："+"用户id："+notice.getId()+";"+"用户uid:"+notice.getUid()+"标题："+notice.getTitle()+"作者："+notice.getAuthor()+"文章内容："+notice.getContent()+"创建时间"+notice.getUpdate_time());
    }

    //测试新增周报
    @Test
    public void testaddNotice(){
        //Notice notice=new Notice(1,1,"title","author","content",new Date());

        NoticesDao ndao=new NoticesDaoImpl();
        Notice notice=new Notice();
        notice.setId(3);
        notice.setUid(2);
        notice.setTitle("title2");
        notice.setAuthor("author2");
        //notice.setContent("文本测试");
        notice.setUpdate_time(new Date());

        ndao.addNotice(notice);

    }

    //测试删除通知
    @Test
    public void testdelNotice(){
        NoticesDao ndao=new NoticesDaoImpl();
        ndao.delNotice(1);
    }
    //测试更新通知
    @Test
    public void testupdateNotice() {
        NoticesDao ndao = new NoticesDaoImpl();

        Notice noticeQueryResult = (Notice) ndao.queryNoticeById(Notice.class, 3);
        System.out.println(noticeQueryResult);
        String preQueryResult=noticeQueryResult.getTitle();
        System.out.println(preQueryResult);
        if (noticeQueryResult == null) {
            System.out.println("查询失败");

        } else {

            noticeQueryResult.setTitle("修改title");
            boolean flag=ndao.updateNotice(noticeQueryResult);
            if(flag) {
                //调用更新方法成功
                Notice noticeQueryResult2 = (Notice) ndao.queryNoticeById(Notice.class, 3);
                String afterQueryResult=noticeQueryResult2.getTitle();
                if (preQueryResult.equals(afterQueryResult)) {
                    System.out.println("更新失败");
                } else {
                    System.out.println("更新成功");
                }
            }

        }
    }



}
