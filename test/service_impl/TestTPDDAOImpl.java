package service_impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.Test;

import entity.TestProjectDetail;
import service.TestProjectDetailDAO;
import service.impl.TestProjectDetailDAOImpl;

public class TestTPDDAOImpl {

	public Session getSession()
	{
		//创建配置对象
		Configuration config = new Configuration().configure();
		//创建服务注册对象
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
		//创建sessionFactory
		SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);
		//创建session对象
		Session session = sessionFactory.getCurrentSession();
		
		return session;
	}
	
	@Test
	public void testAddmethod()
	{
		Session session = this.getSession();
		Transaction tx = session.beginTransaction();
		
		Date date = new Date();
		for (int i=0; i<5; i++ ) {
		    //TestProjectDetail tpd = new TestProjectDetail(i,i,"title","case_state",2.5,5,"bug_type","bug_degree",6,"tester",date,date);
		    
		    //session.save(tpd);
		}
		
		tx.commit();
	}
	
	@Test
	public void testQueryMethod()
	{
		TestProjectDetailDAO tpd = new TestProjectDetailDAOImpl();
		List<TestProjectDetail> l = tpd.queryTPD(1);
		
		for (int i=0;i<l.size(); i++) {
			System.out.println("test:"+l.get(i).getTid());
		}
		
		//System.out.println(l);
	}
	
	@Test
	public void testUpdateMethod()
	{
		TestProjectDetailDAO tpd = new TestProjectDetailDAOImpl();
		Date date = new Date();
		//TestProjectDetail tp = new TestProjectDetail(1,1,"title","case_state",2.5,5,"bug_type","bug_degree",6,"tester",date,date);
		
		//tpd.updateTPD(tp);
	}
	
	@Test
	public void testDelmethod() 
	{
		TestProjectDetailDAO tpd = new TestProjectDetailDAOImpl();
		tpd.delTPD(2);
	}
}
