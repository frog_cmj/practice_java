package service_impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.Test;

import entity.TestProject;
import service.TestProjectDAO;
import service.impl.TestProjectDAOImpl;

//测试项目测试文件
public class TestTPDAOImpl {

	public Session getSession()
	{
		//创建配置对象
		Configuration config = new Configuration().configure();
		//创建服务注册对象
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
		//创建sessionFactory
		SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);
		//创建session对象
		Session session = sessionFactory.getCurrentSession();
		
		return session;
	}
	
	@Test
	public void testAddmethod()
	{
		Session session = this.getSession();
		Transaction tx = session.beginTransaction();
		
		Date date = new Date();
		for (int i=0; i<5; i++ ) {
		    TestProject tp = new TestProject(i,"title"+i,"type","leader","member","qa","progress",date);
		    session.save(tp);
		}
		
		tx.commit();
	}
	
	@Test
	public void testDelmethod() 
	{
		TestProjectDAO tpd = new TestProjectDAOImpl();
		tpd.delTP(2);
	}
	
	@Test
	public void testQueryMethod()
	{
		TestProjectDAO tpd = new TestProjectDAOImpl();
		List<TestProject> l = tpd.queryTP(0);
		
		for (int i=0;i<l.size(); i++) {
			System.out.println(l.get(i).getTitle());
		}
		
		System.out.println(l);
	}
	
	@Test
	public void testUpdateMethod()
	{
		TestProjectDAO tpd = new TestProjectDAOImpl();
		Date date = new Date();
		TestProject tp = new TestProject(3,"titletest","type111","leader22","member33","qa44","progress55",date);
		
		tpd.updateTP(tp);
	}
}
