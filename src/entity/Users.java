package entity;

public class Users {

	private int uid;
	private String username;
	private String password;
	private int auth;

	public Users() {}
	
	public Users(int uid, String username, String password, int auth) {
		//super();
		this.uid = uid;
		this.username = username;
		this.password = password;
		this.auth = auth;
	}

	public int getUid() {
		return uid;
	}
	
	public void setUid(int uid) {
		this.uid = uid;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getAuth() {
		return auth;
	}

	public void setAuth(int auth) {
		this.auth = auth;
	}
	
}
