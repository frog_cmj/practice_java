package entity;

import java.util.Date;

public class Notice {
    private Integer id;
    private Integer uid;
    private String title;
    private String author;
    private String content;
    private Date update_time;

    public Notice(){

    }

    public Notice(Integer id,Integer uid,String title,String author,String content,Date update_time){
        this.id=id;
        this.uid=uid;
        this.title=title;
        this.author=author;
        this.content=content;
        this.update_time=update_time;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public String toString() {
        return "Notice [id=" + id + ", uid=" + uid + ", title=" + title + ", author="+author+",update_time="+update_time+"]";
    }


}
