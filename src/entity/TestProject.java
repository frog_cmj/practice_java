package entity;

import java.util.Date;

public class TestProject {

	private int id;
	private String title;
	private String type;
	private String leader;
	private String member;
	private String qa_leader;
	private String progress;
	private Date reception_time;
	
	private String ajax_date;
	
	public TestProject() {}
	
	public TestProject(int id, String title, String type, String leader, String member, String qa_leader,
			String progress, Date reception_time) {
		super();
		this.id = id;
		this.title = title;
		this.type = type;
		this.leader = leader;
		this.member = member;
		this.qa_leader = qa_leader;
		this.progress = progress;
		this.reception_time = reception_time;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getLeader() {
		return leader;
	}
	
	public void setLeader(String leader) {
		this.leader = leader;
	}
	
	public String getMember() {
		return member;
	}
	
	public void setMember(String member) {
		this.member = member;
	}
	
	public String getQa_leader() {
		return qa_leader;
	}
	
	public void setQa_leader(String qa_leader) {
		this.qa_leader = qa_leader;
	}
	
	public String getProgress() {
		return progress;
	}
	
	public void setProgress(String progress) {
		this.progress = progress;
	}
	
	public Date getReception_time() {
		return reception_time;
	}

	public void setReception_time(Date reception_time) {
		this.reception_time = reception_time;
	}

	public String getAjax_date() {
		return ajax_date;
	}

	public void setAjax_date(String ajax_date) {
		this.ajax_date = ajax_date;
	}
}
