package entity;

import java.sql.Timestamp;
import java.util.Date;

public class Assurance {
	
	private int id;
	private String projectName;
	private String activeType;
	private String content;
	private String address;
	private String remark;
	private String number;
	private Boolean activePass;
	private Boolean problemClose;
	private Timestamp startDate;
	
	private String ajax_date;
	private String ajax_startDate;
	private String ajax_problemClose;
	public String getAjax_problemClose() {
		return ajax_problemClose;
	}

	public void setAjax_problemClose(String ajax_problemClose) {
		this.ajax_problemClose = ajax_problemClose;
	}

	public String getAjax_activePass() {
		return ajax_activePass;
	}

	public void setAjax_activePass(String ajax_activePass) {
		this.ajax_activePass = ajax_activePass;
	}
	private String ajax_activePass;
	public String getAjax_startDate() {
		return ajax_startDate;
	}

	public void setAjax_startDate(String ajax_startDate) {
		this.ajax_startDate = ajax_startDate;
	}

	public String getAjax_EndDate() {
		return ajax_EndDate;
	}

	public void setAjax_EndDate(String ajax_EndDate) {
		this.ajax_EndDate = ajax_EndDate;
	}
	private String ajax_EndDate;
	public String getAjax_date() {
		return ajax_date;
	}

	public void setAjax_date(String ajax_date) {
		this.ajax_date = ajax_date;
	}
	
	public Timestamp getStartDate() {
		return startDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	private Timestamp endDate;
	private Date closeDate;
	
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getActiveType() {
		return activeType;
	}
	public void setActiveType(String activeType) {
		this.activeType = activeType;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Boolean getActivePass() {
		return activePass;
	}
	public void setActivePass(Boolean activePass) {
		this.activePass = activePass;
	}
	public Boolean getProblemClose() {
		return problemClose;
	}
	public void setProblemClose(Boolean problemClose) {
		this.problemClose = problemClose;
	}

	public Date getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	
}
