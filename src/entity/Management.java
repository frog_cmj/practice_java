package entity;

import java.util.Date;
import java.util.List;

import org.omg.CORBA.PRIVATE_MEMBER;

public class Management {
		private int id;
		private String projectName;
		private String active;
		private String activeType;
		private String documentName;
		private String number;
		private Date date;
		private String address;
		private String remark;
		
		private String ajax_date;
		
		public String getAjax_date() {
			return ajax_date;
		}

		public void setAjax_date(String ajax_date) {
			this.ajax_date = ajax_date;
		}

		public Management(int id, String projectName, String active, String activeType,
				String documentName, String number, Date date, String address, String remark) {
			super();
			this.id = id;
			this.projectName =projectName;
			this.active = active;
			this.activeType = activeType;
			this.documentName = documentName;
			this.number = number;
			this.date = date;
			this.address = address;
			this.remark = remark;
			
		}
		
		public Management() {

		}
		
		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getActiveType() {
		return activeType;
	}
	public void setActiveType(String activeType) {
		this.activeType = activeType;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark=remark;
	}

	public Object get(int i) {
		// TODO Auto-generated method stub
		return null;
	}

}
