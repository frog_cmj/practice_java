package entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//测试项目详情表
public class TestProjectDetail {

	private int id;
	private int tid;
	private String title;
	private String test_engineer;//测试工程师
	private Double man_hour;//工时
	private String test_case;//测试用例数据
	private String bug_degree;//缺陷等级数据
	private String bug_type;//缺陷类型数据
	private Date start_time;
	private Date end_time;
	private String ajax_s_time;
	private String ajax_e_time;
    
	private Map<String, Integer> data_param = new HashMap<String, Integer>();//用于存放统计数据（用例、缺陷）
	
	public TestProjectDetail() {};
	
	public TestProjectDetail(int id, int tid, String title, String test_engineer,Double man_hour,
			String test_case, String bug_degree, String bug_type, Date start_time, Date end_time) {
		super();
		this.id = id;
		this.tid = tid;
		this.setTitle(title);
		this.test_engineer = test_engineer;
		this.man_hour = man_hour;
		this.test_case = test_case;
		this.bug_degree = bug_degree;
		this.bug_type = bug_type;
		this.start_time = start_time;
		this.end_time = end_time;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public String getTest_engineer() {
		return test_engineer;
	}

	public void setTest_engineer(String test_engineer) {
		this.test_engineer = test_engineer;
	}

	public Date getStart_time() {
		return start_time;
	}

	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public Date getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAjax_s_time() {
		return ajax_s_time;
	}

	public void setAjax_s_time(String ajax_s_time) {
		this.ajax_s_time = ajax_s_time;
	}

	public String getAjax_e_time() {
		return ajax_e_time;
	}

	public void setAjax_e_time(String ajax_e_time) {
		this.ajax_e_time = ajax_e_time;
	}

	public Double getMan_hour() {
		return man_hour;
	}

	public void setMan_hour(Double man_hour) {
		this.man_hour = man_hour;
	}

	public String getTest_case() {
		return test_case;
	}

	public void setTest_case(String test_case) {
		this.test_case = test_case;
	}

	public String getBug_degree() {
		return bug_degree;
	}

	public void setBug_degree(String bug_degree) {
		this.bug_degree = bug_degree;
	}

	public String getBug_type() {
		return bug_type;
	}

	public void setBug_type(String bug_type) {
		this.bug_type = bug_type;
	}

	public Map<String, Integer> getData_param() {
		return data_param;
	}

	public void setData_param(Map<String, Integer> data_param) {
		this.data_param = data_param;
	}
}
