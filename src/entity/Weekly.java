package entity;

import java.util.Date;

public class Weekly {
	private Integer id;
    private Integer uid;
    private String username;
    private String title;
    private String finished;
    private String notyet;
    private String reason;
    private Integer pct;
    private String nextweek;
    private String comment;
    private Date create_time;
    
    public Weekly() {
    	
    }
    
	public Weekly(Integer id, Integer uid, String username, String title, String finished, String notyet, String reason, Integer pct,
			String nextweek, String comment, Date create_time) {
		super();
		this.id = id;
		this.uid = uid;
		this.username = username;
		this.title = title;
		this.finished = finished;
		this.notyet = notyet;
		this.reason = reason;
		this.pct = pct;
		this.nextweek = nextweek;
		this.comment = comment;
		this.create_time = create_time;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFinished() {
		return finished;
	}

	public void setFinished(String finished) {
		this.finished = finished;
	}

	public String getNotyet() {
		return notyet;
	}

	public void setNotyet(String notyet) {
		this.notyet = notyet;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getPct() {
		return pct;
	}

	public void setPct(Integer pct) {
		this.pct = pct;
	}

	public String getNextweek() {
		return nextweek;
	}

	public void setNextweek(String nextweek) {
		this.nextweek = nextweek;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	@Override
	public String toString() {
		return "Weekly [id=" + id + ", uid=" + uid + ", title=" + title + ", finished=" + finished + ", notyet="
				+ notyet + ", reason=" + reason + ", pct=" + pct + ", nextweek=" + nextweek
				+ ", create_time=" + create_time + "]";
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
