package action;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import entity.Weekly;
import service.WeeklyDAO;
import service.impl.WeeklyDAOImpl;

//导出类
public class ExportAction extends SuperAction {

	private static final long serialVersionUID = 1L;
	
	//导出周报相关
	public void export() throws Exception
	{
		String[][] content = new String[6][8];//导出内容主体
		String[] title = {"标题","已完成","未完成","完成百分比","未完成原因","下周计划","作者","时间"};
		String fileName = "导出周报"+System.currentTimeMillis()+".xls";
		String sheetName = "周报内容";
		
		String id = request.getParameter("id");
		String[] ids = id.split(",");
		
		WeeklyDAO wdao = new WeeklyDAOImpl();
		
		if (ids.length > 0) {
			for (int i = 0; i < ids.length; i++) {
				List<Weekly> list = wdao.queryWeeklyById(Integer.parseInt(ids[i]));
				
				content[i][0] = list.get(0).getTitle();
				content[i][1] = list.get(0).getFinished();
				content[i][2] = list.get(0).getNotyet();
				content[i][3] = list.get(0).getPct().toString();
				content[i][4] = list.get(0).getReason();
				content[i][5] = list.get(0).getNextweek();
				content[i][6] = session.getAttribute("loginUserName").toString();
				content[i][7] = new Date().toString();
			}
		}
		
		HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook(sheetName, title, content, null);
		
		//响应到客户端
	    this.setResponseHeader(fileName);
	    OutputStream os = response.getOutputStream();
	    wb.write(os);
        os.flush();
        os.close();

	}
	
	
	//发送响应流方法
    public void setResponseHeader(String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(),"ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
