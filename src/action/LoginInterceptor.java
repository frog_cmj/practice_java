package action;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptor extends AbstractInterceptor{

	private static final long serialVersionUID = 1L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		ActionContext user = invocation.getInvocationContext(); 
		Map<?, ?> session = user.getSession();
		String username = (String) session.get("loginUserName");
		
		if (null != username && !"".equals(username)) {
			System.out.println("已登录用户:"+username);
			return invocation.invoke();
		}
		
		System.out.println("请登录");
		return "error_login";
	}

}
