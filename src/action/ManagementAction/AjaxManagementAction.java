package action.ManagementAction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import action.SuperAction;
import entity.Management;
import net.sf.json.JSONArray;
import service.ManagementDao;
import service.impl.ManagementDaoImpl;




public class AjaxManagementAction extends SuperAction{
	private static final long serialVersionUID = 1L;
	
	private String management;

	public String getManagement() {
		return management;
	}

	public void setManagement(String management) {
		this.management = management;
	}
	
	  //查询项目内容
		public String querySingleManagement()
		{
			Integer id = Integer.parseInt(request.getParameter("id"));
			
			ManagementDao mdao = new ManagementDaoImpl();
			List<Management> list = mdao.queryManagementByid(id);
			list.get(0).setAjax_date(list.get(0).getDate().toString());
			list.get(0).setDate(null);//时间转换成json报错，找不到合适的转换方法，先去掉
		    management = JSONArray.fromObject(list).toString();
			
			return "query_single_management";
		}
		
		
		//更新单条项目内容
		public String updateSingleManagement() throws ParseException 
		{
			int id = Integer.parseInt(request.getParameter("id"));
			String projectName = request.getParameter("projectName");
			String active = request.getParameter("active");
			String activeType = request.getParameter("activeType");
			String documentName= request.getParameter("documentName");
			String number  = request.getParameter("number");
			String address = request.getParameter("address");
			String remark = request.getParameter("remark");
			String x=null;
	        x=request.getParameter("date");
	        java.sql.Date date=java.sql.Date.valueOf(x);
			
			
			Management m= new Management(); 
			m.setId(id);
			m.setProjectName(projectName);
			m.setActive(active);
			m.setActiveType(activeType);
			m.setDocumentName(documentName);
			m.setNumber(number);
			m.setAddress(address);
			m.setRemark(remark);
			m.setDate(date);
			ManagementDao mdao = new ManagementDaoImpl();
			
			if (mdao.updateManagement(m)) {
				management = "修改成功！";
			} else {
				management = "修改失败！";
			}
			
			return "update_single_management";
		}
		
		
		//删除项目
		public String deleteManagement()
		{

			ManagementDao mdao = new ManagementDaoImpl();
			
			String id = request.getParameter("id");
			String[] ids = id.split(",");
			
			for (String i : ids) {
				if (i.length() > 0) {
					mdao.deleteManagement(Integer.parseInt(i));
				}
			}
			management = "删除成功";
			return "delete_management";
		}
		
		
	
}
