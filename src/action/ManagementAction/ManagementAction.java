package action.ManagementAction;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

//管理action类
import action.SuperAction;
import entity.Assurance;
import entity.Management;
import service.AssuranceDao;
import service.ManagementDao;
import service.impl.AssuranceDaoImpl;
import service.impl.ManagementDaoImpl;



public class ManagementAction extends SuperAction{
	
	private Management management= new Management();
	
	private static final long serialVersionUID=1L;

	//查询所有项目的动作
	public String query() {
		
		ManagementDao mDao=new ManagementDaoImpl();
		List<Management> list=mDao.queryAllManagements();
		//放入session中
		if(list!=null&&list.size()>0) {
			session.setAttribute("management_list", list);
			System.out.println(list);
		}
		request.getSession().removeAttribute("assurance_list");
		AssuranceDao adao=new AssuranceDaoImpl(); 
		List<Assurance> list2=adao.queryAllAssurance();
		//放入session中
		if(list2!=null&&list2.size()>0) {
			session.setAttribute("assurance_list", list2);
			System.out.println(list2);
		}
		return "query_management_success";
		
	}
	
	//添加项目
	public String addmanagement() throws ParseException
	{
		/*management.setId(Integer.parseInt(request.getParameter("id")));*///保存项目内容
		management.setProjectName(request.getParameter("projectName"));
		management.setActive(request.getParameter("active"));
		management.setActiveType(request.getParameter("activeType"));
		management.setDocumentName(request.getParameter("documentName"));
		management.setNumber(request.getParameter("number"));
		management.setAddress(request.getParameter("address"));
		management.setRemark(request.getParameter("remark"));
	
		String x=null;
        x=request.getParameter("date");
        java.sql.Date date=java.sql.Date.valueOf(x);
        management.setDate(date);
        ManagementDao mdao = new ManagementDaoImpl();
		mdao.addManagement(management);
	/*	session.setAttribute("", mdao);
*/
		
		return "add_management_success";
	}
	
		
/*	//删除项目
	public String delete()
	{
		ManagementDao mdao = new ManagementDaoImpl();
		int id=Integer.parseInt(request.getParameter("id"));
		mdao.deleteManagement(id);

		return "delete_management_success";
	}
	*/
	/*//修改项目资料
	public String modify() {
		ManagementDao mdao = new ManagementDaoImpl();
		String projectName=request.getParameter("projectName");
		Management m=mdao.queryManagementByid(projectName);
		//保存在会话中
		session.setAttribute("modify_management",m);
		return "modify_management_success";
	}
	*/
	
	
	
	//分页
	    ManagementDao ps = new ManagementDaoImpl();  
	  
	    List<Management> pageManagementList = new ArrayList();  
	  
	    private int rowsPerPage = 10;// 每页显示几条  
	  
	    private int page = 1; // 默认当前页  
	  
	    private int totalPage;// 总共多少页  
	  
	    private int managementNum=10;// 总过多少条  
	  
	    public String show() {  
	  
	        System.out.println("Page:" + page);  
	        pageManagementList = ps.findManagementByPage(page, rowsPerPage);  
	        totalPage = ps.getManagementTotalPage(rowsPerPage);  
	        managementNum = ps.getManagementNum();  
	  
	        return "success";  
	    }  
	  
	    public int getPage() {  
	        return page;  
	    }  
	  
	    public void setPage(int page) {  
	        this.page = page;  
	    }  
	  
	    public int getRowsPerPage() {  
	        return rowsPerPage;  
	    }  
	  
	    public void setRowsPerPage(int rowsPerPage) {  
	        this.rowsPerPage = rowsPerPage;  
	    }  
	  
	    public int getTotalPage() {  
	        return totalPage;  
	    }  
	  
	    public void setTotalPage(int totalPage) {  
	        this.totalPage = totalPage;  
	    }  
	  
	    public List<Management> getPageManagementList() {  
	        return pageManagementList;  
	    }  
	  
	    public void setPageManagementList(List<Management> pageManagementList) {  
	        this.pageManagementList = pageManagementList;  
	    }  
	  
	    public int getManagementNum() {  
	        return managementNum;  
	    }  
	  
	    public void setManagementNum(int managementNum) {  
	        this.managementNum = managementNum;  
	    }  
	  
	

	
	
}
