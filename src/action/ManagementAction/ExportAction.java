package action.ManagementAction;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import action.ExcelUtil;
import action.SuperAction;
import entity.Management;
import service.ManagementDao;
import service.impl.ManagementDaoImpl;


//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

//导出类
public class ExportAction extends SuperAction {

	private static final long serialVersionUID = 1L;
	
	//导出周报相关
	public void export() throws Exception
	{
		String[][] content = new String[6][8];//导出内容主体
		String[] title = {"项目名称","活动","活动类型","文档/代码名称","版本号","实际建立的日期","输出存放位置","备注"};
		String fileName = "导出项目"+System.currentTimeMillis()+".xls";
		String sheetName = "项目内容";
		
		String id = request.getParameter("id");
		String[] ids = id.split(",");
		
		ManagementDao mdao = new ManagementDaoImpl();
		
		if (ids.length > 0) {
			for (int i = 0; i < ids.length; i++) {
				List<Management> list = mdao.queryManagementByid(Integer.parseInt(ids[i]));//查询周报内容
				//保存周报代码
				content[i][0] = list.get(0).getProjectName();
				content[i][1] = list.get(0).getActive();
				content[i][2] = list.get(0).getActiveType();
				content[i][3] = list.get(0).getDocumentName();
				content[i][4] = list.get(0).getNumber();
				Date x= list.get(0).getDate();
				java.text.SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd ");
				String date = formatter.format(x);//格式化数据
				content[i][5] = date;
				content[i][6] = list.get(0).getAddress();
				content[i][7] = list.get(0).getRemark();	
			}
		}
		
		HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook(sheetName, title, content, null);
		
		//响应到客户端
	    this.setResponseHeader(fileName);
	    OutputStream os = response.getOutputStream();
	    wb.write(os);
        os.flush();
        os.close();

	}
	
	
	//发送响应流方法
    public void setResponseHeader(String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(),"ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
