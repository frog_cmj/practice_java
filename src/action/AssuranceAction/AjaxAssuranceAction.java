package action.AssuranceAction;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.hql.internal.ast.tree.BooleanLiteralNode;

import action.SuperAction;
import entity.Assurance;
import net.sf.json.JSONArray;
import service.AssuranceDao;
import service.impl.AssuranceDaoImpl;




public class AjaxAssuranceAction extends SuperAction {
	private static final long serialVersionUID = 1L;
	
	private String assurance;
	
	
	public String getAssurance() {
		return assurance;
	}


	public void setAssurance(String assurance) {
		this.assurance = assurance;
	}


	//删除项目
	public String deleteAssurance()
	{
		AssuranceDao adao = new AssuranceDaoImpl();
		
		String id = request.getParameter("id");
		String[] ids = id.split(",");
		
		for (String i : ids) {
			if (i.length() > 0) {
				adao.deleteAssurance(Integer.parseInt(i));
			}
		}
		assurance = "删除成功";
		return "delete_assurance";
	}
	
	
			//查询项目
			public String querySingleAssurance()
			{
				int id = Integer.parseInt(request.getParameter("id"));
				AssuranceDao adao = new AssuranceDaoImpl();
				List<Assurance> list = adao.queryAssuranceByid(id);
				String m,n;
				if(list.get(0).getActivePass().equals(true)) {
					 m="通过";
				}else {
					 m="不通过";
				}
				if(list.get(0).getProblemClose().equals(true)) {
					 n="已闭环";
				}else {
					 n="未闭环";
				}	
				
				list.get(0).setAjax_activePass(m);
				list.get(0).setAjax_problemClose(n);
				list.get(0).setAjax_date(list.get(0).getCloseDate().toString());
				list.get(0).setAjax_startDate(list.get(0).getStartDate().toString());
				list.get(0).setAjax_EndDate(list.get(0).getEndDate().toString());
				list.get(0).setStartDate(null);//时间转换成json报错，找不到合适的转换方法，先去掉
				list.get(0).setCloseDate(null);
				list.get(0).setEndDate(null);
				assurance = JSONArray.fromObject(list).toString();
				
				return "query_single_assurance";
			}
			
			
			
		//更新单条项目内容
			public String updateSingleAssurance() throws ParseException
			{
				int id = Integer.parseInt(request.getParameter("id"));
				String projectName = request.getParameter("projectName");
				String activeType = request.getParameter("activeType");
				String content= request.getParameter("content");
				String number  = request.getParameter("number");
				String address = request.getParameter("address");
				String remark = request.getParameter("remark");
				Boolean m;
				Boolean n;
				String problemClose = request.getParameter("problemClose");
				String activePass = request.getParameter("activePass");
				if(problemClose.equals("已闭环")) {
					m=true;
				}else {
					m=false;
				}
				if(activePass.equals("通过")) {
					n=true;
				}else {
					n=false;
				}			
				String x,y,z;	
			    x=request.getParameter("startDate");
			    java.text.SimpleDateFormat formatter =
			    new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
			    java.util.Date m1= formatter.parse(x);  
			    Timestamp startDate =new Timestamp(m1.getTime());
				y=request.getParameter("endDate");
				java.text.SimpleDateFormat formatter1 =
			    new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
				java.util.Date m2= formatter1.parse(y); 
				Timestamp endDate =new Timestamp(m2.getTime());
				
			    z=request.getParameter("closeDate");
			    java.sql.Date CloseDate=java.sql.Date.valueOf(z);	
				
				Assurance a= new Assurance();
				a.setActivePass(n);
				a.setProblemClose(m);
				a.setId(id);
				a.setProjectName(projectName);
				a.setActiveType(activeType);
				a.setContent(content);
				a.setNumber(number);
				a.setAddress(address);
				a.setRemark(remark);
				a.setStartDate(startDate);
				a.setCloseDate(CloseDate);
				a.setEndDate(endDate);
				
				AssuranceDao adao = new AssuranceDaoImpl();
				
				if (adao.updateAssurance(a)) {
					assurance = "修改成功！";
				} else {
					assurance = "修改失败！";
				}
				
				return "update_single_assurance";
			}
		
}
