package action.AssuranceAction;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import action.ExcelUtil;
import action.SuperAction;
import entity.Assurance;
import service.AssuranceDao;
import service.impl.AssuranceDaoImpl;



//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

//导出类
public class ExportAction extends SuperAction {

	private static final long serialVersionUID = 1L;
	
	//导出周报相关
	public void export() throws Exception
	{
		String[][] content = new String[6][11];//导出内容主体
		String[] title = {"项目名称","活动类型","详细内容","活动开始时间","活动结束时间","活动是否通过","遗留问题个数","问题是否闭环","问题闭环的时间","输出存放的位置","备注"};
		String fileName = "导出项目"+System.currentTimeMillis()+".xls";
		String sheetName = "项目内容";
		
		String id = request.getParameter("id");
		String[] ids = id.split(",");
		
		AssuranceDao mdao = new AssuranceDaoImpl();
		
		if (ids.length > 0) {
			for (int i = 0; i < ids.length; i++) {
				List<Assurance> list = mdao.queryAssuranceByid(Integer.parseInt(ids[i]));//查询项目内容
				//保存项目代码
				content[i][0] = list.get(0).getProjectName();
				content[i][1] = list.get(0).getActiveType();
				content[i][2] = list.get(0).getContent();
				Date y= list.get(0).getStartDate();
				java.text.SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss ");
				String startdate = formatter.format(y);//格式化数据
				content[i][3] = startdate;
				Date z= list.get(0).getEndDate();
				java.text.SimpleDateFormat formatter1 = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
				String enddate = formatter1.format(z);//格式化数据
				content[i][4] = enddate;
				
				Boolean type=list.get(0).getActivePass();
				String m = null;
				if(type==true)
					m= "是";	
				else 
					m="否";
				content[i][5] = m;
				content[i][6] = list.get(0).getNumber();
				Boolean close=list.get(0).getProblemClose();
				String n = null;
				if(close==true)
					n= "是";	
				else 
					n="否";
				content[i][7] =n;
				Date x= list.get(0).getCloseDate();
				java.text.SimpleDateFormat formatter11 = new SimpleDateFormat( "yyyy-MM-dd ");
				String date = formatter11.format(x);//格式化数据
				content[i][8] = date;
				content[i][9] = list.get(0).getAddress();
				content[i][10] = list.get(0).getRemark();
			}
		}
		
		HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook(sheetName, title, content, null);
		
		//响应到客户端
	    this.setResponseHeader(fileName);
	    OutputStream os = response.getOutputStream();
	    wb.write(os);
        os.flush();
        os.close();

	}
	
	
	//发送响应流方法
    public void setResponseHeader(String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(),"ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
