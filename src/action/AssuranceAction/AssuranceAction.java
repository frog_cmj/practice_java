package action.AssuranceAction;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import action.SuperAction;
import entity.Assurance;
import service.AssuranceDao;
import service.impl.AssuranceDaoImpl;




public class AssuranceAction extends SuperAction{
    private Assurance assurance = new Assurance();
	
	private static final long serialVersionUID=1L;
	
	
	//查询所有项目的动作
	public String query() {
		AssuranceDao adao=new AssuranceDaoImpl(); 
		List<Assurance> list=adao.queryAllAssurance();
		//放入session中
		if(list!=null&&list.size()>0) {
			session.setAttribute("assurance_list", list);
			System.out.println(list);
		}
		return "query_assurance_success";
	}
	
	
	
	//添加项目
	public String addassurance() throws ParseException
	{
		assurance.setProjectName(request.getParameter("projectName"));
		assurance.setActiveType(request.getParameter("activeType"));
		assurance.setContent(request.getParameter("content"));
		assurance.setNumber(request.getParameter("number"));
		assurance.setAddress(request.getParameter("address"));
		assurance.setRemark(request.getParameter("remark"));
		
		String m=request.getParameter("activePass") ;		
		boolean d;
		if(m.equals("通过"))
			d= true;
		else
			d = false;
		assurance.setActivePass(d);	
		
		String n=request.getParameter("problemClose");
		boolean c;
		if(n.equals("已闭环"))
			c= true;
		else
			c = false;
		assurance.setProblemClose(c);
		String x,y,z;
		
		
       x=request.getParameter("startDate");
        java.text.SimpleDateFormat formatter =
                new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
        java.util.Date m1= formatter.parse(x);  
        Timestamp startDate =new Timestamp(m1.getTime());
		assurance.setStartDate(startDate); 
		y=request.getParameter("endDate");
		java.text.SimpleDateFormat formatter1 =
	                new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
		java.util.Date m2= formatter1.parse(y); 
		Timestamp endDate =new Timestamp(m2.getTime());
		assurance.setEndDate(endDate);
		
	    z=request.getParameter("closeDate");
	    java.sql.Date CloseDate=java.sql.Date.valueOf(z);
		assurance.setCloseDate(CloseDate);
		
		AssuranceDao adao = new AssuranceDaoImpl();
		adao.addAssurance(assurance);
	/*	session.setAttribute("", mdao);
*/
		
		return "add_assurance_success";
	}
	




		//删除项目
		public String delete()
		{
			AssuranceDao adao = new AssuranceDaoImpl();
			int id=Integer.parseInt(request.getParameter("id"));
			adao.deleteAssurance(id);

			return "delete_assurance_success";
		}
	
}
