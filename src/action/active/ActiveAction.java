package action.active;

import action.SuperAction;
import entity.ActiveShow;
import service.ActiveShowDao;
import service.impl.ActiveShowDaoImpl;

import java.util.Date;

public class ActiveAction extends SuperAction {


    //新增活动
    public String addActive() {
        ActiveShowDao activeShowDao = new ActiveShowDaoImpl();
        ActiveShow activeShow = new ActiveShow();
        activeShow.setId(Integer.parseInt(request.getParameter("id")));
        activeShow.setAid(Integer.parseInt(request.getParameter("aid")));
        activeShow.setTitle(request.getParameter("title"));
        activeShow.setContent(request.getParameter("content"));
        activeShow.setUpdate_time(new Date());
        activeShowDao.addActive(activeShow);
        return "add_active_success";
    }

    //删除活动
    public String delActive(){
        ActiveShowDao activeShowDao=new ActiveShowDaoImpl();
        boolean result=activeShowDao.delActive(Integer.parseInt(request.getParameter("id")));
        if(result){

            return "del_notice_success";
        }
        return "del_notice_failed";
    }

    //更新活动
    public String updateActive(){
        ActiveShowDao activeShowDao=new ActiveShowDaoImpl();
        int the_id=Integer.parseInt(request.getParameter("id"));
        ActiveShow the_activeshow=activeShowDao.queryActive(ActiveShow.class,the_id);
        session.setAttribute("updateActive",the_activeshow);
        return "update_active_success";
    }
}

