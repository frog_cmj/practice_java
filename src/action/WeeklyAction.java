package action;

import java.util.Date;
import java.util.List;

import entity.Weekly;
import service.WeeklyDAO;
import service.impl.WeeklyDAOImpl;

public class WeeklyAction extends SuperAction {

	private static final long serialVersionUID = 1L;

	private Weekly weekly = new Weekly();
	
	//查询所有周报
	public String query()
	{
		int page = 1;//防止空指针发生异常，先赋值ֵ
		int page_o = 1;
		String weekly_user_search_o = "";//其他周报用户名搜索
		String weekly_start_time_o = "";//时间搜索
		String weekly_end_time_o = "";
		
		int uid = Integer.parseInt(session.getAttribute("loginUserUid").toString());//登陆人的id
		int auth = Integer.parseInt(session.getAttribute("loginUserAuth").toString());//登陆人权限
		
		session.setAttribute("weekly_list", null);//先清空session，防止分页溢出的时候显示之前的数据
		session.setAttribute("weekly_list_o", null);
		session.setAttribute("request_o", 2);//是否是其他周报请求,每次请求重置
		
		//我的周报分页
		if (request.getParameter("page") != null && !"".equals(request.getParameter("page")) && !"null".equals(request.getParameter("page"))) {
			page = Integer.parseInt(request.getParameter("page"));
		}
		
		//其他周报分页
		if (request.getParameter("page_o") != null && !"".equals(request.getParameter("page_o")) && !"null".equals(request.getParameter("page_o"))) {
			page_o = Integer.parseInt(request.getParameter("page_o"));
			session.setAttribute("request_o", 1);
		}
		//其他周报用户名搜索
		if (request.getParameter("weekly_user_search_o") != null && !"null".equals(request.getParameter("weekly_user_search_o"))) {
			if (!"".equals(request.getParameter("weekly_user_search_o"))) {
				weekly_user_search_o = request.getParameter("weekly_user_search_o");
				session.setAttribute("weekly_user_search_o", request.getParameter("weekly_user_search_o").toString());
			} else {
				session.removeAttribute("weekly_user_search_o");
			}
			session.setAttribute("request_o", 1);
		} else if (session.getAttribute("weekly_user_search_o") != null && session.getAttribute("weekly_user_search_o") != "") {
			weekly_user_search_o = session.getAttribute("weekly_user_search_o").toString();//�����ҳʱ��������
		}
		//时间搜索条件
		if (request.getParameter("weekly_start_time_o") != null && !"null".equals(request.getParameter("weekly_start_time_o"))) {
			if (!"".equals(request.getParameter("weekly_start_time_o"))) {
				session.setAttribute("weekly_start_time_o", request.getParameter("weekly_start_time_o").toString());
				weekly_start_time_o = request.getParameter("weekly_start_time_o").toString();
				session.setAttribute("request_o", 1);

			} else {
				session.removeAttribute("weekly_start_time_o");
			}
		} else if (session.getAttribute("weekly_start_time_o") != null && session.getAttribute("weekly_start_time_o") != "") {
			weekly_start_time_o = session.getAttribute("weekly_start_time_o").toString();
		}
		if (request.getParameter("weekly_end_time_o") != null && !"null".equals(request.getParameter("weekly_end_time_o"))) {
			if (!"".equals(request.getParameter("weekly_end_time_o"))) {
				session.setAttribute("weekly_end_time_o", request.getParameter("weekly_end_time_o").toString());
				weekly_end_time_o = request.getParameter("weekly_end_time_o").toString();
				session.setAttribute("request_o", 1);
			} else {
				session.removeAttribute("weekly_end_time_o");
			}
		} else if (session.getAttribute("weekly_end_time_o") != null && session.getAttribute("weekly_end_time_o") != "") {
			weekly_end_time_o = session.getAttribute("weekly_end_time_o").toString();
		}
		
		//获取个人周报
        this.persistWeekly(page_o, uid);
		//获取其他周报
		this.persistOtherWeekly(auth, page_o, uid, weekly_user_search_o, weekly_start_time_o, weekly_end_time_o);
		
		session.setAttribute("current_page", page);
		session.setAttribute("current_page_o", page_o);
		
		return "query_weekly_success";
	}
	
	//持久化周报
	public void persistWeekly(int page, int uid)
	{
		WeeklyDAO wdao = new WeeklyDAOImpl();
		List<Weekly> list = wdao.queryAllWeekly(page, uid);
		
		if (list != null && list.size() > 0) {
			session.setAttribute("weekly_list", list);
		} else {
			session.setAttribute("weekly_list", null);
		}
	}
	
	//持久化其他周报
	public void persistOtherWeekly(int auth, int page_o, int uid, String weekly_user_search_o, String weekly_start_time_o, String weekly_end_time_o)
	{
		if (auth == 1) {
			WeeklyDAO wdao = new WeeklyDAOImpl();
			List<Weekly> list_o = wdao.queryOtherWeekly(page_o, uid, weekly_user_search_o, weekly_start_time_o, weekly_end_time_o, 0);
			if (list_o != null && list_o.size() > 0) {
				session.setAttribute("weekly_list_o", list_o);
			} else {
				session.setAttribute("weekly_list_o", null);
			}
		} else {
			session.setAttribute("weekly_list_o", null);
		}
	}
	
	//添加周报
	public String addweekly()
	{
		weekly.setTitle(request.getParameter("title"));
        weekly.setFinished(request.getParameter("finished"));
        weekly.setNotyet(request.getParameter("notyet"));
        weekly.setReason(request.getParameter("reason"));
        weekly.setNextweek(request.getParameter("nextweek"));
        weekly.setPct(Integer.parseInt(request.getParameter("pct")));
        weekly.setUid(Integer.parseInt(session.getAttribute("loginUserUid").toString()));
        weekly.setUsername(session.getAttribute("loginUserName").toString());
        weekly.setCreate_time(new Date());
        
		WeeklyDAO wdao = new WeeklyDAOImpl();
		wdao.addWeekly(weekly);
		
		session.setAttribute("current_page", 1);
		session.setAttribute("current_page_o", 1);
		session.setAttribute("request_o", 2);//是否是其他周报请求
		
		return "add_weekly_success";
	}
	
	
}
