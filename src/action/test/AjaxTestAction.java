package action.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import action.SuperAction;
import entity.TestProject;
import net.sf.json.JSONArray;
import service.TestProjectDAO;
import service.impl.TestProjectDAOImpl;

public class AjaxTestAction extends SuperAction  {

	private static final long serialVersionUID = 1L;

	private String tp;
	
	public String getTp() {
		return tp;
	}

	public void setTp(String tp) {
		this.tp = tp;
	}
	
	public String queryTP()
	{
		session.setAttribute("tp_detail_request", 0);//非测试数据页面请求
		int id = Integer.parseInt(request.getParameter("id"));
		TestProjectDAO tpdao = new TestProjectDAOImpl();
		
		List<TestProject> list = tpdao.queryTP(id);
		
		list.get(0).setAjax_date(list.get(0).getReception_time().toString());//时间对象转存字符串
		list.get(0).setReception_time(null);
		
		tp = JSONArray.fromObject(list).toString();
		return "ajax_get_tp";
	}
	
	public String updateTP()
	{
		session.setAttribute("tp_detail_request", 0);//非测试数据页面请求
		Date reception = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		String title = request.getParameter("title");
		String type = request.getParameter("type");
		String leader = request.getParameter("leader");
		String member = request.getParameter("member");
		String qa_leader = request.getParameter("qa_leader");
		String progress = request.getParameter("progress");
		try {
			reception = sdf.parse(request.getParameter("reception").toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		TestProjectDAO tpdao = new TestProjectDAOImpl();
		
		if (request.getParameter("id")!=null && !"".equals(request.getParameter("id"))) {//修改
			int id = Integer.parseInt(request.getParameter("id"));
			
			TestProject tpj = new TestProject(id,title,type,leader,member,qa_leader,progress,reception);
			
			if (tpdao.updateTP(tpj)) {
				tp = "修改成功!";
			} else {
				tp = "修改失败!";
			}
		} else {//新增
			TestProject tpj = new TestProject(0,title,type,leader,member,qa_leader,progress,reception);
			
			if (tpdao.addTP(tpj)) {
				tp = "添加成功！";
			} else {
				tp = "添加失败！";
			}
		}
		
		return "ajax_update_tp";
	}
	
	public String delTP()
	{
		session.setAttribute("tp_detail_request", 0);//非测试数据页面请求
		int id = Integer.parseInt(request.getParameter("id"));
		
		TestProjectDAO tpdao = new TestProjectDAOImpl();
		if (tpdao.delTP(id)) {
			tp = "删除成功！";
		} else {
			tp = "删除失败！";
		}
		
		return "ajax_del_tp";
	}
	
}
