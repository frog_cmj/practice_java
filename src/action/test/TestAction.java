package action.test;

import java.util.List;

import action.SuperAction;
import entity.TestProject;
import service.TestProjectDAO;
import service.impl.TestProjectDAOImpl;

public class TestAction extends SuperAction {
    
	private static final long serialVersionUID = 1L;

	public String test()
    {
    	System.out.println("test");
    	return "test";
    }
    
    public String queryTP()
    {
    	if (session.getAttribute("tp_detail_request") == null) {
    		session.setAttribute("tp_detail_request", 0);//非测试数据页面请求
    	}
    	
    	TestProjectDAO tp = new TestProjectDAOImpl();
    	List<TestProject> list = tp.queryTP(0);
    	
    	if (list != null && list.size() > 0) {
    		session.setAttribute("tp_list", list);
    	}
    	
    	return "tp_index";
    }
    
}
