package action.test;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
//import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import action.SuperAction;
import entity.TestProjectDetail;
import net.sf.json.JSONArray;
import service.TestProjectDetailDAO;
import service.impl.TestProjectDetailDAOImpl;

public class AjaxTestDetailAction extends SuperAction{

	private static final long serialVersionUID = 1L;
	private String tpd;
	Map<String, Integer> test_case = new HashMap<String, Integer>();
	Map<String, Integer> bug_degree = new HashMap<String, Integer>();
	Map<String, Integer> bug_type = new HashMap<String, Integer>();
	Map<String, Integer> data_param = new HashMap<String, Integer>();//存放前三个图数据集合
	private static ObjectMapper MAPPER = new ObjectMapper();
	
	public String queryAllTPD() throws JsonParseException, JsonMappingException, IOException
	{
		int tid = Integer.parseInt(request.getParameter("id"));

		TestProjectDetailDAO tpddao = new TestProjectDetailDAOImpl();
		List<TestProjectDetail> list = tpddao.queryAllTPD(tid);
		
		this.statisticTPD(this.organizeData(list));//饼图数据生成
		session.setAttribute("tp_detail_list", this.organizeData(list));
		session.setAttribute("tp_detail_request", 1);//测试数据页面请求
		session.setAttribute("tp_detail_request_id", tid);//上一次请求的tid
		
		tpd = "ajax_query_tpd";
		return "ajax_query_tpd";
	}
	
	//测试数据统计
	public void statisticTPD(List<TestProjectDetail> list)
	{
		Map<String, Integer> tpdmap = new HashMap<String, Integer>();
		
		tpdmap.put("case_pass", 0);//通过的用例
		tpdmap.put("case_block", 0);//阻塞的用例
		tpdmap.put("case_not_pass", 0);//未通过的用例
		tpdmap.put("bug_type_interface", 0);//缺陷类型-界面
		tpdmap.put("bug_type_require", 0);//缺陷类型-需求
		tpdmap.put("bug_type_performance", 0);//缺陷类型-性能
		tpdmap.put("bug_type_feature", 0);//缺陷类型-功能
		tpdmap.put("bug_type_safety", 0);//缺陷类型-安全
		tpdmap.put("bug_degree_s", 0);//缺陷等级-轻微
		tpdmap.put("bug_degree_n", 0);//缺陷等级-普通
		tpdmap.put("bug_degree_h", 0);//缺陷等级-严重
		
		for (int i = 0;i < list.size(); i++) {
			tpdmap.put("case_pass", tpdmap.get("case_pass")+list.get(i).getData_param().get("pass"));
			tpdmap.put("case_not_pass", tpdmap.get("case_not_pass")+list.get(i).getData_param().get("not_pass"));
			tpdmap.put("case_block", tpdmap.get("case_block")+list.get(i).getData_param().get("block"));
			tpdmap.put("bug_type_feature", tpdmap.get("bug_type_feature")+list.get(i).getData_param().get("feature"));
			tpdmap.put("bug_type_performance", tpdmap.get("bug_type_performance")+list.get(i).getData_param().get("performance"));
			tpdmap.put("bug_type_interface", tpdmap.get("bug_type_interface")+list.get(i).getData_param().get("interface"));
			tpdmap.put("bug_type_require", tpdmap.get("bug_type_require")+list.get(i).getData_param().get("requirement"));
			tpdmap.put("bug_type_safety", tpdmap.get("bug_type_safety")+list.get(i).getData_param().get("safety"));
			tpdmap.put("bug_degree_s", tpdmap.get("bug_degree_s")+list.get(i).getData_param().get("s"));
			tpdmap.put("bug_degree_n", tpdmap.get("bug_degree_n")+list.get(i).getData_param().get("n"));
			tpdmap.put("bug_degree_h", tpdmap.get("bug_degree_h")+list.get(i).getData_param().get("h"));
		}
		
		session.setAttribute("statistic_tpd", tpdmap);
	}
	
	public String queryTPD() throws JsonParseException, JsonMappingException, IOException
	{
		int id = Integer.parseInt(request.getParameter("id"));
		
		TestProjectDetailDAO tpddao = new TestProjectDetailDAOImpl();
		List<TestProjectDetail> list = tpddao.queryTPD(id);
		
		list.get(0).setAjax_s_time(list.get(0).getStart_time().toString());
		list.get(0).setAjax_e_time(list.get(0).getEnd_time().toString());
		list.get(0).setStart_time(null);
		list.get(0).setEnd_time(null);
		
		tpd = JSONArray.fromObject(this.organizeData(list)).toString();
		return "ajax_query_single_tpd";
	}

	public String updateTPD() throws IOException
	{
		Date start_time = null;
		Date end_time = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			start_time = sdf.parse(request.getParameter("tpd_s_time").toString());
			end_time = sdf.parse(request.getParameter("tpd_e_time").toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int tid = Integer.parseInt(request.getParameter("tpd_tid"));
		String title = request.getParameter("tpd_title");
		String tester = request.getParameter("tpd_tester");
		double man_hour = Double.parseDouble(request.getParameter("tpd_man_hour"));
		
		Map<String, Integer> test_case = new HashMap<String, Integer>();
		Map<String, Integer> bug_degree = new HashMap<String, Integer>();
		Map<String, Integer> bug_type = new HashMap<String, Integer>();
		
		test_case.put("pass", Integer.parseInt(request.getParameter("tpd_case_pass")));
		test_case.put("not_pass", Integer.parseInt(request.getParameter("tpd_case_notpass")));
		test_case.put("block", Integer.parseInt(request.getParameter("tpd_case_block")));
		bug_degree.put("s", Integer.parseInt(request.getParameter("tpd_bug_degree_s")));
		bug_degree.put("n", Integer.parseInt(request.getParameter("tpd_bug_degree_n")));
		bug_degree.put("h", Integer.parseInt(request.getParameter("tpd_bug_degree_h")));
		bug_type.put("feature", Integer.parseInt(request.getParameter("tpd_bug_type_feature")));
		bug_type.put("performance", Integer.parseInt(request.getParameter("tpd_bug_type_performance")));
		bug_type.put("interface", Integer.parseInt(request.getParameter("tpd_bug_type_interface")));
		bug_type.put("requirement", Integer.parseInt(request.getParameter("tpd_bug_type_requirement")));
		bug_type.put("safety", Integer.parseInt(request.getParameter("tpd_bug_type_safety")));//组织成map类型，保存时转化成json格式的String类型
		
		TestProjectDetailDAO tpddao = new TestProjectDetailDAOImpl();
		
		if (request.getParameter("tpd_id")!=null && !"".equals(request.getParameter("tpd_id"))) {//修改
		    int id = Integer.parseInt(request.getParameter("tpd_id"));
		    TestProjectDetail testpd = new TestProjectDetail(id,tid,title,tester,man_hour,MAPPER.writeValueAsString(test_case),
                                           MAPPER.writeValueAsString(bug_degree),MAPPER.writeValueAsString(bug_type),start_time,end_time);
            if (tpddao.updateTPD(testpd)) {
            	tpd = "修改成功！";
            } else {
            	tpd = "修改失败！";
            }
		    
		} else {//新增
		    TestProjectDetail testpd = new TestProjectDetail(0,tid,title,tester,man_hour,MAPPER.writeValueAsString(test_case),
		    		                       MAPPER.writeValueAsString(bug_degree),MAPPER.writeValueAsString(bug_type),start_time,end_time);
            if (tpddao.addTPD(testpd)) {
            	tpd = "添加成功！";
            } else {
            	tpd = "添加失败！";
            }
		}
		
		List<TestProjectDetail> list = tpddao.queryAllTPD(tid);

		session.setAttribute("tp_detail_list", this.organizeData(list));
		session.setAttribute("tp_detail_request", 1);//测试数据页面请求
		session.setAttribute("tp_detail_request_id", tid);//上一次请求的tid
		
		return "ajax_update_tpd";
	}
	
	public String delTPD() throws JsonParseException, JsonMappingException, IOException
	{
		int id = Integer.parseInt(request.getParameter("id"));
		int tid = Integer.parseInt(request.getParameter("tid"));
		TestProjectDetailDAO tpddao = new TestProjectDetailDAOImpl();
		
		if(tpddao.delTPD(id)) {
			tpd = "删除成功！";
		} else {
			tpd = "删除失败！";
		}
		
		List<TestProjectDetail> list = tpddao.queryAllTPD(tid);

		session.setAttribute("tp_detail_list", this.organizeData(list));
		session.setAttribute("tp_detail_request", 1);//测试数据页面请求
		session.setAttribute("tp_detail_request_id", tid);//上一次请求的tid
		
		return "ajax_del_tpd";
	}
	
	public List<TestProjectDetail> organizeData(List<TestProjectDetail> list) throws JsonParseException, JsonMappingException, IOException {
		for (int i = 0; i < list.size(); i++) {
			test_case = MAPPER.readValue(list.get(i).getTest_case(), new TypeReference<Map<String, Integer>>(){});
			bug_degree = MAPPER.readValue(list.get(i).getBug_degree(), new TypeReference<Map<String, Integer>>(){});
			bug_type = MAPPER.readValue(list.get(i).getBug_type(), new TypeReference<Map<String, Integer>>(){});
			
			data_param.putAll(test_case);
			data_param.putAll(bug_type);
			data_param.putAll(bug_degree);
			list.get(i).getData_param().putAll(data_param);
			data_param.clear();
		}
		
		return list;
	}

	public String getTpd() {
		return tpd;
	}


	public void setTpd(String tpd) {
		this.tpd = tpd;
	}
}
