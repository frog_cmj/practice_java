package action;

import java.util.List;

import entity.Users;
import service.UsersDAO;
import service.impl.UsersDAOImpl;

public class AjaxUserAction extends SuperAction {

	private static final long serialVersionUID = 1L;
	private String uRes;

	public String getuRes() {
		return uRes;
	}

	public void setuRes(String uRes) {
		this.uRes = uRes;
	}
	
	public String doModifyPassword()
	{
		int uid = Integer.parseInt(session.getAttribute("loginUserUid").toString());
		String pwd = request.getParameter("pwd");
		
		UsersDAO udao = new UsersDAOImpl();
		
		if (udao.modifyPassword(uid, pwd)) {
			uRes = "success";
		} else {
			uRes = "failed";
		}
		
		return "do_modify_password";
	}
	
	public String delUser()
	{
		int uid = Integer.parseInt(request.getParameter("uid"));
		
		UsersDAO udao = new UsersDAOImpl();
		if (udao.delUser(uid)) {
			//重新保存所有用户信息
			List<Users> list = udao.getUsers();
			
			session.setAttribute("allUsers", list);
			
			uRes = "删除成功！";
		} else {
			uRes = "删除失败！";
		}
		
		return "del_user_success";
	}
	
}
