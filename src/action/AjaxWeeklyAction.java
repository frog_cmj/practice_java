package action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import entity.Users;
import entity.Weekly;
import net.sf.json.JSONArray;
import service.UsersDAO;
import service.WeeklyDAO;
import service.impl.UsersDAOImpl;
import service.impl.WeeklyDAOImpl;

public class AjaxWeeklyAction extends SuperAction {

	private static final long serialVersionUID = 1L;
	
	private String weekly;
	
	private static ObjectMapper MAPPER = new ObjectMapper();

	public String getWeekly() {
		return weekly;
	}

	public void setWeekly(String weekly) {
		this.weekly = weekly;
	}

	//查询单条周报内容
	public String querySingleWeekly()
	{
		int id = Integer.parseInt(request.getParameter("id"));
		
		WeeklyDAO wdao = new WeeklyDAOImpl();
		List<Weekly> list = wdao.queryWeeklyById(id);
		
		list.get(0).setCreate_time(null);//时间转换成json报错，找不到合适的转换方法，先去掉
		weekly = JSONArray.fromObject(list).toString();
		
		return "query_single_weekly";
	}
	
	//更新单条周报内容
	public String updateSingleWeekly()
	{
		int id = Integer.parseInt(request.getParameter("id"));
		String title = request.getParameter("title");
		String finished = request.getParameter("finished");
		String notyet = request.getParameter("notyet");
		String reason = request.getParameter("reason");
		String nextweek = request.getParameter("nextweek");
		int pct = Integer.parseInt(request.getParameter("pct"));
		String comment = request.getParameter("comment");
		
		Weekly w = new Weekly();
		w.setId(id);
		w.setTitle(title);
		w.setFinished(finished);
		w.setNotyet(notyet);
		w.setReason(reason);
		w.setNextweek(nextweek);
		w.setComment(comment);
		w.setPct(pct);
		
		WeeklyDAO wdao = new WeeklyDAOImpl();
		
		if (wdao.updateWeekly(w)) {
			weekly = "修改成功！";
		} else {
			weekly = "修改失败！";
		}
		
		return "update_single_weekly";
	}
	
	//删除周报
	public String delWeekly()
	{
		WeeklyDAO wdao = new WeeklyDAOImpl();
		
		String id = request.getParameter("id");
		String[] ids = id.split(",");
		
		for (String i : ids) {
			if (i.length() > 0) {
				wdao.delWeekly(Integer.parseInt(i));
			}
		}
		
		weekly = "删除成功！";
		return "del_weekly";
	}
	
	//查询7天未提交用户
	public String uncommittedUser() throws ParseException, JsonProcessingException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		Calendar date = Calendar.getInstance();
		date.setTime(today);
		date.set(Calendar.DATE, date.get(Calendar.DATE) - 7);
		Date endDate = sdf.parse(sdf.format(date.getTime()));//7天前的时间
		
		WeeklyDAO wdao = new WeeklyDAOImpl();
		List<Weekly> list = wdao.queryOtherWeekly(0, 0, "", sdf.format(endDate).toString(), "", 1);
		
		Map<String, String> umap = this.getUncommitedUser(list);
		weekly = MAPPER.writeValueAsString(umap).toString();
		return "uncommitted_user";
	}
	
	public Map<String, String> getUncommitedUser(List<Weekly> list)
	{
		Map<String, String> usermap = new HashMap<String, String>();
		
		UsersDAO udao = new UsersDAOImpl();
		List<Users> userlist = udao.getUsers();
		
		for (int i = 0; i<userlist.size(); i++) {
			usermap.put(userlist.get(i).getUsername(), userlist.get(i).getUsername());
		}
		
		for (int i = 0; i<list.size(); i++) {
			usermap.remove(list.get(i).getUsername());
		}
		
		return usermap;
	}
	
}
