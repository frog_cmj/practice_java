package service;

import java.util.List;

import entity.TestProjectDetail;

public interface TestProjectDetailDAO {

	//查询单条数据
	public List<TestProjectDetail> queryTPD(int id);
	
	//查询单个项目的所有详情
	public List<TestProjectDetail> queryAllTPD(int tid);
	
	//更新测试项目数据
	public Boolean updateTPD(TestProjectDetail tpd);
	
	//删除测试详情
	public Boolean delTPD(int id);
	
	//添加测试项目详情
	public Boolean addTPD(TestProjectDetail tpd);
}
