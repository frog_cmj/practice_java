package service;

import java.sql.Date;
import java.util.List;

import entity.Management;

public interface ManagementDao {
	// 查询所有的项目   
	public List<Management> queryAllManagements();
	
	// 根据项目的序号查询项目信息
	public List<Management>  queryManagementByid(int id);
	
	// 添加项目
	public boolean addManagement(Management m);
	
	//修改项目信息
	public boolean updateManagement(Management m);
	
	//删除项目的信息
	public boolean deleteManagement(int id);

	public List<Management> queryAllManagement(int id, String projectName,
						String active,String activeType,String documentName,
						String number,Date date,String address,String remark);
		
	
	//分页
	 public int getManagementTotalPage(int rowsPerPage);  
	  
	 public List<Management> findManagementByPage(int page, int rowsPerPage);  
	  
	 public int getManagementNum();

	

	

	
}
