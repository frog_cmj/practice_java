package service;

//import java.util.Date;
import java.util.List;

import entity.Weekly;

//周报业务逻辑接口
public interface WeeklyDAO {

	//查询所有周报内容
	public List<Weekly> queryAllWeekly(int page, int uid);
	
	//根据周报id查询单条周报
	public List<Weekly> queryWeeklyById(int id);
	
	//添加周报
	public Boolean addWeekly(Weekly w);
	
	//删除周报
	public Boolean delWeekly(int id);
	
	//修改周报
	public Boolean updateWeekly(Weekly w);

	//查询其他周报
	public List<Weekly> queryOtherWeekly(int page, int uid, String weekly_user_search_o, String weekly_start_time_o, String weekly_end_time_o, int uncommited);
	
}
