package service;

import entity.ActiveShow;

import java.util.List;

public interface ActiveShowDao {

    //分页查询全部
    public List<ActiveShow> quertAllActive(int page, int id);

    //不按分页查询全部
    public List<ActiveShow> quertAllActive(int id);

    //根据id查找活动
    public ActiveShow queryActive(Class clazz, int id);

    //新增活动
    public boolean addActive(ActiveShow activeShow);

    //删除活动
    public boolean delActive(int id);

    //更新活动
    public boolean updateActive(ActiveShow activeShow);
}
