package service;

import java.util.List;

import entity.Assurance;
import entity.Management;


public interface AssuranceDao {

	
	    // 查询所有的项目   
		public List<Assurance> queryAllAssurance();
		
		// 根据项目的序号查询项目信息
		public List<Assurance>  queryAssuranceByid(int id);
		
		// 添加项目
		public boolean addAssurance(Assurance a);
		
		//修改项目信息
		public boolean updateAssurance(Assurance a);
		
		//删除项目的信息
		public boolean deleteAssurance(int id);

	
}
