package service;

import java.util.List;

import entity.TestProject;

public interface TestProjectDAO {

	//查询测试项目
	public List<TestProject> queryTP(int id);
	
	//删除测试项目
	public Boolean delTP(int id);
	
	//新增测试项目
	public Boolean addTP(TestProject tp);
	
	//更新测试项目
	public Boolean updateTP(TestProject tp);
}
