package service;

import java.util.List;

import entity.Users;

//用户业务逻辑接口
public interface UsersDAO {

	//用户登陆类型
	public List<Users> usersLogin(Users u);
	
	//修改密码
	public Boolean modifyPassword(int uid, String pwd);
	
	//查询所有用户
	public List<Users> getUsers();
	
	//添加用户
	public Boolean addUser(String username, String password, int auth);
	//删除用户
	public Boolean delUser(int uid);
}
