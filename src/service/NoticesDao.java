package service;

import entity.Notice;

import java.util.List;

public interface NoticesDao {

    //不分页查询所有通知
    public  List<Notice> queryAllNotices();

    //分页查询所有通知
    public List<Notice> queryAllNotices(int page, int id);

    //查询某个通知
    public Object queryNoticeById(Class clazz, int id);

    //新增通知
    public Boolean addNotice(Notice notice);

    //删除通知
    public Boolean delNotice(int id);

    //更新通知
    public Boolean updateNotice(Notice notice);

    //查询其他通知
   // public List<Weekly> queryOtherNotice(int page, int uid);

}
