package service;

import entity.PersonnelInfo;

import javax.management.Query;
import java.util.List;

public interface PersonnelInfoDao {

    //分页查询所有人人员信息
    public List<PersonnelInfo> queryAllPersonnelInfo(int page,int pid);

    //不分页查询所有人员信息
    public List<PersonnelInfo> queryAllPersonnelInfo(int pid);

    //查找人员信息
    public PersonnelInfo queryPersonnelInfo(Class clazz,int pid);

    //添加人员信息
    public boolean addPersonnelInfo(PersonnelInfoDao personnelInfoDao);

    //删除人员信息
    public boolean delPersonnelInfo(int pid);

    //更新人员信息
    public boolean updatePersonnelInfo(PersonnelInfoDao personnelInfoDao);




}
