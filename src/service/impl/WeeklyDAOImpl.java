package service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import db.MyHibernateSessionFactory;
import entity.Weekly;
import service.WeeklyDAO;

public class WeeklyDAOImpl implements WeeklyDAO {

	@Override
	public List<Weekly> queryAllWeekly(int page, int uid) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		List<Weekly> list = null;
		String hql = "";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "from Weekly where uid = ? order by id desc";
			Query query = session.createQuery(hql);
			query.setParameter(0, uid);
			list = query.setFirstResult((page-1)*6).setMaxResults(6).list();
			
			tx.commit();
			
			return list;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return null;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}
	
	@Override
	public List<Weekly> queryOtherWeekly(int page, int uid, String weekly_user_search_o, String weekly_start_time_o, String weekly_end_time_o, int uncommited) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		List<Weekly> list = null;
		String hql = "";
		int param_username = 0;////记录有几个搜索得参数
		
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "from Weekly where uid != ?";
			
			if (weekly_user_search_o != null && weekly_user_search_o.length() > 0) {
				hql += " and username = ?";
				param_username++;
			}
			if (weekly_start_time_o.length() > 0) {
				hql += " and create_time >= '"+weekly_start_time_o+"'";
			}
			if (weekly_end_time_o.length() > 0) {
				hql += " and create_time <= '"+weekly_end_time_o+"'";
			}
			hql += " order by id desc";
			
			Query query = session.createQuery(hql);
			query.setParameter(0, uid);
			
			if (param_username > 0) {
				query.setParameter(param_username, weekly_user_search_o);
			}
			
			if (uncommited == 1) {
				list = query.list();
			} else {
				list = query.setFirstResult((page-1)*6).setMaxResults(6).list();
			}
			
			tx.commit();
			
			return list;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return null;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public List<Weekly> queryWeeklyById(int id) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		List<Weekly> list = null;
		String hql = "";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "from Weekly where id = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			list = query.list();
			
			tx.commit();
			
			return list;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return null;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean addWeekly(Weekly w) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			session.save(w);
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
		
		
		
	}

	@Override
	public Boolean delWeekly(int id) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "delete from Weekly where id = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			
			query.executeUpdate();
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean updateWeekly(Weekly w) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "update Weekly w "
					+ "set w.title = ?, w.finished = ?, w.notyet = ?,"
					+ "w.reason = ?, w.nextweek = ?, w.comment = ?, w.pct = ?"
					+ "where w.id = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, w.getTitle());
			query.setParameter(1, w.getFinished());
			query.setParameter(2, w.getNotyet());
			query.setParameter(3, w.getReason());
			query.setParameter(4, w.getNextweek());
			query.setParameter(5, w.getComment());
			query.setParameter(6, w.getPct());
			query.setParameter(7, w.getId());
            
			query.executeUpdate();
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
		
	}
	
}
