package service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import db.MyHibernateSessionFactory;
import entity.Assurance;
import entity.Management;
import service.AssuranceDao;

public class AssuranceDaoImpl implements AssuranceDao{

	@Override
	public List<Assurance> queryAllAssurance() {
		// TODO Auto-generated method stub
		Transaction tx=null;
		List<Assurance> list=null;
		String hql="";
		try {
			Session session=MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			hql="from Assurance order by id desc";
			tx=session.beginTransaction();
			Query query=session.createQuery(hql);
			list=query.list();
			tx.commit();
			return list;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return list;
		}finally {
			if(tx!=null) {
				tx=null;
			}
		}
	}

	@Override
	public List<Assurance> queryAssuranceByid(int id) {
		Transaction tx=null;
		List<Assurance> list = null;
		String hql="";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "from Assurance where id = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			list = query.list();
			
			tx.commit();
			
			return list;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return null;
		}finally {
			if(tx!=null) {
				tx=null;
			}
		}
	}
	//增加项目
	@Override
	public boolean addAssurance(Assurance a) {	
			// TODO Auto-generated method stub
			Transaction tx = null;
			
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			session.save(a);
			
			tx.commit();
			
			return true;
		}
	@Override
	public boolean updateAssurance(Assurance a) {
		// TODO Auto-generated method stub
				Transaction tx = null;
				String hql = "";
				try {
					Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
					tx = session.beginTransaction();
					
					hql = "update Assurance a "
							+ "set a.projectName = ?, a.activeType = ?, a.content = ?, a.number = ?, a.problemClose = ?, a.activePass = ?,"
							+ "a.address = ?, a.remark = ?, a.startDate = ?, a.endDate = ?, a.closeDate = ?"
							+ "where a.id = ?";
					Query query = session.createQuery(hql);
					query.setParameter(0, a.getProjectName());
					query.setParameter(1, a.getActiveType());
					query.setParameter(2, a.getContent());
					query.setParameter(3, a.getNumber());
					query.setParameter(4, a.getProblemClose());
					query.setParameter(5, a.getActivePass());
					query.setParameter(6, a.getAddress());
					query.setParameter(7, a.getRemark());
					query.setParameter(8, a.getStartDate());
					query.setParameter(9, a.getEndDate());
					query.setParameter(10, a.getCloseDate());
					query.setParameter(11, a.getId());
		            
					query.executeUpdate();
					
					tx.commit();
					
					return true;
				} catch (Exception ex) {
					ex.printStackTrace();
					//tx.commit();
					return false;
				} finally {
					if (tx != null) {
						tx = null;
					}
				}
				
			}
			


	//删除项目
	@Override
	public boolean deleteAssurance(int id) {	
			// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "delete from Assurance where id = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			
			query.executeUpdate();
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

}
