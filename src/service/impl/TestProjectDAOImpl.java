package service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import db.MyHibernateSessionFactory;
import entity.TestProject;
import service.TestProjectDAO;

public class TestProjectDAOImpl implements TestProjectDAO{

	@Override
	public List<TestProject> queryTP(int id) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		List<TestProject> list = null;
		try
		{
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			hql = "from TestProject";
			
			if (id != 0) {
				hql += " where id='" + id + "'"; 
			}
			
			Query query = session.createQuery(hql);

			list = query.list();
			tx.commit();

			return list;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally 
		{
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean delTP(int id) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "delete from TestProject where id = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			
			query.executeUpdate();
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean addTP(TestProject tp) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.save(tp);
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean updateTP(TestProject tp) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			session.update(tp);
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

}
