package service.impl;

import db.MyHibernateSessionFactory;
import entity.ActiveShow;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import service.ActiveShowDao;

import java.util.Date;
import java.util.List;

public class ActiveShowDaoImpl implements ActiveShowDao {
    //分页查询全部
    public List<ActiveShow> quertAllActive(int page, int id){
        Session session= MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
        return null;
    }

    //不按分页查询全部
    public List<ActiveShow> quertAllActive(int id) {
        Transaction tx = null;
        List<ActiveShow> list = null;
        String hql = "";
        try {
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            Query query = session.createQuery("from ActiveShow ");
            list=query.list();
            for(ActiveShow activeShow:list){
                System.out.println(activeShow);
        }

        }catch (Exception ex){
            ex.printStackTrace();
            tx.commit();

        }finally {
            if(tx!=null){
                tx=null;
            }
        }
        return list;
    }


    //根据id查找活动
    public ActiveShow queryActive(Class clazz, int id) {
            Session session= MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            Transaction tx=null;
            tx=session.beginTransaction();
            try {
                ActiveShow activeShow = (ActiveShow) session.get(clazz, id);
                if (activeShow != null) {
                    return activeShow;
                } else {
                    System.out.println("查询结果为空！");
                    return null;
                }
            }finally {
                if(session!=null){
                    session.close();
                }



        }
    }

    //新增活动
    public boolean addActive(ActiveShow activeShow){
        Transaction tx=null;
        Session session= MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
        tx=session.beginTransaction();
        try {
            activeShow.setId(1);
            activeShow.setAid(1);
            activeShow.setTitle("title");
            activeShow.setContent("content");
            activeShow.setUpdate_time(new Date());

            session.save(activeShow);
            tx.commit();

        }finally {
            if(session!=null){
                session=null;
            }
        }


        return true;
    }

    //删除活动
    public boolean delActive(int aid){
        Transaction tx = null;
        String hql = "";
        try {
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();

            hql = "delete from ActiveShow where aid = ?";
            Query query = session.createQuery(hql);
            query.setParameter(0, aid);

            query.executeUpdate();

            tx.commit();

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.commit();
            return false;
        } finally {
            if (tx != null) {
                tx = null;
            }
        }
    }

    //更新活动
    public boolean updateActive(ActiveShow activeShow){
        Transaction tx=null;
        try{
            Session session=MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx=session.beginTransaction();
            session.update(activeShow);
            tx.commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            tx.commit();
            return false;
        }finally {
            if(tx!=null){
                tx=null;
            }
        }

    }
}
