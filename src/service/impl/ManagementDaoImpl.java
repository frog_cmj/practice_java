package service.impl;

import java.sql.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import db.MyHibernateSessionFactory;
import entity.Management;
import service.ManagementDao;

public class ManagementDaoImpl implements ManagementDao{
	
	//查询所有项目信息
	@Override
	public List<Management> queryAllManagements() {
		// TODO Auto-generated method stub
		Transaction tx=null;
		List<Management> list=null;
		String hql="";
		try {
			Session session=MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			hql="from Management order by id desc";
			tx=session.beginTransaction();
			Query query=session.createQuery(hql);
			list=query.list();
			tx.commit();
			return list;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return list;
		}finally {
			if(tx!=null) {
				tx=null;
			}
		}
	}
	
	
	//查询项目资料
	@Override
	public List<Management> queryManagementByid(int id) {
		Transaction tx=null;
		List<Management> list = null;
		String hql="";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "from Management where id = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			list = query.list();
			
			tx.commit();
			
			return list;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return null;
		}finally {
			if(tx!=null) {
				tx=null;
			}
		}
	}

	//增加项目
	@Override
	public boolean addManagement(Management m) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		
		Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
		tx = session.beginTransaction();
		
		session.save(m);
		
		tx.commit();
		
		return true;
	}

	@Override
	public boolean updateManagement(Management m) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		try {
			
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "update Management m "
					+ "set m.projectName = ?, m.active = ?, m.activeType = ?,"
					+ "m.documentName = ?, m.number = ?, m.address = ?, m.remark = ?, m.date = ?"
					+ "where m.id = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, m.getProjectName());
			query.setParameter(1, m.getActive());
			query.setParameter(2, m.getActiveType());
			query.setParameter(3, m.getDocumentName());
			query.setParameter(4, m.getNumber());
			query.setParameter(5, m.getAddress());
			query.setParameter(6, m.getRemark());
			query.setParameter(7, m.getDate());
			query.setParameter(8, m.getId());
			
			
			query.executeUpdate();
			session.update(m);
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
		
	}
	
	//删除项目
		@Override
		public boolean deleteManagement(int id) {	
				// TODO Auto-generated method stub
			Transaction tx = null;
			String hql = "";
			try {
				Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
				tx = session.beginTransaction();
				
				hql = "delete from Management where id = ?";
				Query query = session.createQuery(hql);
				query.setParameter(0, id);
				
				query.executeUpdate();
				
				tx.commit();
				
				return true;
			} catch (Exception ex) {
				ex.printStackTrace();
				//tx.commit();
				return false;
			} finally {
				if (tx != null) {
					tx = null;
				}
			}
		}
	

	
	
	
	//分页
	public List<Management> findManagementByPage(int page, int rowsPerPage) {  
		Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
        Query query = session.createQuery("from Management order by desc");  
        query.setMaxResults(rowsPerPage); // 每页最多显示几条  
        query.setFirstResult((page - 1) * rowsPerPage); // 每页从第几条记录开始  
        List<Management> list = query.list();  
        for (int i = 0; i < list.size(); i++) {  
            System.out.println("findManagementByPage:"  
                    + list.get(i).getProjectName());  
        }  
  
        session.close();  
  
        return list;  
    }  
  
    /** 
     * 共多少页计划数据 
     */  
    public int getManagementTotalPage(int rowsPerPage) {  
        // System.out.println("rowsPerPage:" + rowsPerPage);  
        int rows = 0;  
        String hql = "select count(*) from Management";  
        Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession(); 
        Query query = session.createQuery(hql);  
  
        rows = ((Integer) query.iterate().next()).intValue();  
        // System.out.println("rows:" + rows);  
        session.close();  
        if (rows % rowsPerPage == 0) {  
            return rows / rowsPerPage;  
        } else {  
            return rows / rowsPerPage + 1;  
        }  
    }  
  
    public int getManagementNum() {  
        String hql = "select count(*) from Management ";  
        int rows = 0;  
        Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
        Query query = session.createQuery(hql);  
  
        rows = ((Integer) query.iterate().next()).intValue();  
  
        session.close();  
        return rows;  
    }  
  
    /** 
     * 条件查询后返回的计划总页数 
     */  
    public int getManagementTotalPage(int rowsPerPage, String type, String search) {  
        int rows = 0;  
        Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession(); 
        String hql = "select count(*) from Management m where m." + type  
                + " like :type";  
        Query query = session.createQuery(hql);  
        query.setString("type", "%" + search + "%");  
  
        rows = ((Integer) query.iterate().next()).intValue();  
        // System.out.println("rows:" + rows);  
        session.close();  
        if (rows % rowsPerPage == 0) {  
            return rows / rowsPerPage;  
        } else {  
            return rows / rowsPerPage + 1;  
        }  
    }  
  
    /** 
     * 条件查询后返回的计划数据总数 
     */  
    public int getManagementNum(String type, String search) {  
        int rows = 0;  
        Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
        String hql = "select count(*) from Management m where m." + type  
                + " like :type";  
        Query query = session.createQuery(hql);  
        query.setString("type", "%" + search + "%");  
  
        rows = ((Integer) query.iterate().next()).intValue();  
        // System.out.println("rows:" + rows);  
        session.close();  
        return rows;  
    }


	@Override
	public List<Management> queryAllManagement(int id, String projectName, String active, String activeType,
			String documentName, String number, Date date, String address, String remark) {
		// TODO Auto-generated method stub
		return null;
	}




    
}  
	
	

