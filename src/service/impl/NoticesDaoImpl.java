package service.impl;

import db.MyHibernateSessionFactory;
import entity.Notice;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import service.NoticesDao;

import java.util.Date;
import java.util.List;

public class NoticesDaoImpl implements NoticesDao {

    //不分页查询所有通知
    @Override
    public List<Notice> queryAllNotices() {
        Transaction tx=null;
        List<Notice> list=null;
        String hql="";
        try {
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            Query query=session.createQuery("from Notice ");
            list=query.list();
            for(Notice notice:list){
                System.out.println(notice);
            }

        }catch (Exception ex){
            ex.printStackTrace();
            tx.commit();

        }finally {
            if(tx!=null){
                tx=null;
            }
        }
        return list;
    }

    //分页查询所有通知
    public List<Notice> queryAllNotices(int page, int id) {

        Transaction tx = null;
        List<Notice> noticelist = null;
        String hql = "";
        try {
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();

            hql = "from Notice where uid = ? order by id desc";
            Query query = session.createQuery(hql);
            query.setParameter(0, id);
            noticelist = query.setFirstResult((page - 1) * 6).setMaxResults(6).list();
            //提交事务
            tx.commit();

            return noticelist;
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.commit();
            return noticelist;
        } finally {
            if (tx != null) {
                tx = null;
            }
        }

    }

    //根据id查询某个通知
    public Notice queryNoticeById(Class clazz, int id) {

        Transaction tx = null;
        Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
        tx = session.beginTransaction();
        try {
            Notice notice = (Notice) session.get(clazz, id);
            //return notice;
            if (notice != null) {
                return notice;
            }else {
                System.out.println("查询结果为空！");
                return null;
            }

        } finally {
            {
                if (session != null) {
                    session.close();
                }
            }
        }
    }


    //新增通知
    public Boolean addNotice(Notice notice) {
        Transaction tx = null;

        Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
        tx = session.beginTransaction();
        notice.setId(1);
        notice.setUid(1);
        notice.setTitle("title");
        notice.setAuthor("author");
        notice.setUpdate_time(new Date());

        session.save(notice);

        tx.commit();
        return true;

    }

    //删除通知
    public Boolean delNotice(int id){
        Transaction tx = null;
        String hql = "";
        try {
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();

            hql = "delete from Notice where id = ?";
            Query query = session.createQuery(hql);
            query.setParameter(0, id);

            query.executeUpdate();

            tx.commit();

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.commit();
            return false;
        } finally {
            if (tx != null) {
                tx = null;
            }
        }

    }

    //更新通知（有问题）
    public Boolean updateNotice(Notice notice){
        Transaction tx = null;
        //String hql = "";
        try {
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();

            String hql="";



            hql = "update Notice notice "

                    + "set notice.title = ?, notice.author = ?, notice.content = ?,notice.update_time = ?"
                    + "where notice.id = ?";
            Query query=session.createQuery(hql);

            query.setParameter(0, notice.getTitle());
            query.setParameter(1, notice.getAuthor());
            query.setParameter(2, notice.getContent());
            query.setParameter(3, notice.getUpdate_time());
            query.setParameter(4, notice.getId());

            query.executeUpdate();


            session.update(notice);


            tx.commit();

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.commit();
            return false;
        } finally {
            if (tx != null) {
                tx = null;
            }
        }

    }

}
