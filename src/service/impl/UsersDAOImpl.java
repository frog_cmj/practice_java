package service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import db.MyHibernateSessionFactory;
import entity.Users;
import service.UsersDAO;

public class UsersDAOImpl implements UsersDAO{

	@Override
	public List<Users> usersLogin(Users u) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		List<Users> list = null;
		try
		{
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			hql = "from Users where username=? and password=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, u.getUsername());
			query.setParameter(1, u.getPassword());

			list = query.list();
			tx.commit();

			return list;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally 
		{
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean modifyPassword(int uid, String pwd) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		try
		{
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			hql = "update Users u set u.password = ? where u.uid = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, pwd);
			query.setParameter(1, uid);
			query.executeUpdate();
			
			tx.commit();

			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		finally 
		{
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public List<Users> getUsers() {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		List<Users> list = null;
		try
		{
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			hql = "from Users";
			Query query = session.createQuery(hql);

			list = query.list();
			tx.commit();

			return list;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally 
		{
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean addUser(String username, String password, int auth) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		try
		{
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			Users u = new Users(1, username, password, auth);
			session.save(u);
			tx.commit();

			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		finally 
		{
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean delUser(int uid) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "delete from Users where uid = ?";
			Query query = session.createQuery(hql);
			query.setParameter(0, uid);
			
			query.executeUpdate();
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			//tx.commit();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

}
