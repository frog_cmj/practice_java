package service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import db.MyHibernateSessionFactory;
import entity.TestProjectDetail;
import service.TestProjectDetailDAO;

public class TestProjectDetailDAOImpl implements TestProjectDetailDAO{

	@Override
	public List<TestProjectDetail> queryTPD(int id) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		List<TestProjectDetail> list = null;
		try
		{
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			hql = "from TestProjectDetail";
			
			if (id != 0) {
				hql += " where id = '" + id + "'";
			}
			
			Query query = session.createQuery(hql);

			list = query.list();
			tx.commit();

			return list;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally 
		{
			if (tx != null) {
				tx = null;
			}
		}
	}
	
	@Override
	public List<TestProjectDetail> queryAllTPD(int tid) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		List<TestProjectDetail> list = null;
		try
		{
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			hql = "from TestProjectDetail";
			
			if (tid != 0) {
				hql += " where tid = '" + tid + "'";
			}
			
			Query query = session.createQuery(hql);

			list = query.list();
			tx.commit();

			return list;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally 
		{
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean updateTPD(TestProjectDetail tpd) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			session.update(tpd);
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean delTPD(int id) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		String hql = "";
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			
			hql = "delete from TestProjectDetail where id = '" + id + "'";
			Query query = session.createQuery(hql);
			
			query.executeUpdate();
			
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

	@Override
	public Boolean addTPD(TestProjectDetail tpd) {
		// TODO Auto-generated method stub
		Transaction tx = null;
		try {
			Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
			session.save(tpd);
			tx.commit();
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			if (tx != null) {
				tx = null;
			}
		}
	}

}
